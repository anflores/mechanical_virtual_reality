String MANIPULATOR_NAME = "laser_trigger";
#define NUM_DIG_OUTPUT 2

short DO_PINS[NUM_DIG_OUTPUT] = {2,3};
char *DO_NAMES[] = {"trigger1"};
int pin_status[NUM_DIG_OUTPUT] = {0, 0};


float PUBLISH_DATA_FREQ = 0;



short commaIx;
short prevCommaIx;

String publishMsg = "";          // string to publish data
String inputString = "";         // string to hold incoming data
bool stringComplete = false;  // whether the string is complete
bool startPublishing = false;

long timerPublisher;


void cmdCB(){
  if (stringComplete){
    if (inputString.substring(0,4) == String("info")){
      String infoMsg = MANIPULATOR_NAME + String(",") + String(NUM_DIG_OUTPUT);
      for(short i=0; i < NUM_DIG_OUTPUT; i++){
        infoMsg +=  String(",") + String(DO_NAMES[i]);
      }
      Serial.println(infoMsg);
      delay(5);
      inputString = "";
      stringComplete = false;
      return;
    }
    else if (inputString.substring(0,5) == String("start")){
      PUBLISH_DATA_FREQ = inputString.substring(5).toFloat();
      startPublishing = true;
      inputString = "";
      stringComplete = false;

      return;
    }
    commaIx = -1;
    prevCommaIx = -1;
    for(short i=0; i < NUM_DIG_OUTPUT; i++){
       
        commaIx = inputString.indexOf(',', commaIx + 1);
        pin_status[i] = inputString.substring(prevCommaIx + 1, commaIx).toInt();
        analogWrite(DO_PINS[i], pin_status[i]);
        prevCommaIx = commaIx;
           
    }

    inputString = "";
    stringComplete = false;
  }
   
}



void setup() {
  Serial.begin(57600);
  for(int i=0; i< NUM_DIG_OUTPUT; i++){
    pinMode(DO_PINS[i], OUTPUT);
  }
  timerPublisher = millis();

}

void loop() {
    if (startPublishing){
      publishData();
    }
    cmdCB();
}

void publishData(){
  long delayTime = millis() - timerPublisher ;

  if (delayTime > 1000/PUBLISH_DATA_FREQ){
        publishMsg = "";
        for(short i=0; i < NUM_DIG_OUTPUT; i++){
            publishMsg += String(pin_status[i]);
            if (i < (NUM_DIG_OUTPUT-1)){
              publishMsg += ",";
            }
        }
        Serial.println(publishMsg);

        timerPublisher = millis();
    }
}


void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
