#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import Marker
import time
from settings import *
import numpy as np
from laser_trigger_interface import laserTrigger
import threading
import mechanical_arena_driver.msg as mm
from mechanical_arena_driver.srv import set_mode, set_modeResponse, set_modeRequest
import actionlib
import tf

from camera_view.srv import set_directory, set_directoryResponse, set_directoryRequest
from camera_view.srv import set_recording, set_recordingResponse, set_recordingRequest
from mechanical_arena_driver.srv import set_log_directory, set_log_directoryRequest, set_log_directoryResponse
from mechanical_arena_driver.srv import set_gains, set_gainsResponse, set_gainsRequest

import datetime
import os
import shutil
import rospkg
rospack = rospkg.RosPack()


now = datetime.datetime.now()
datet = now.strftime("%m-%d-%Y-%H-%M")
DIR_LOGGING = '/media/nci_la/New Volume/mechanical_arena_data/place_learning/' + 'experiment_' + datet + '/'

if not os.path.exists(DIR_LOGGING):
    os.makedirs(DIR_LOGGING)


pkg_dir = rospack.get_path('place_learning')
shutil.copy(pkg_dir + '/scripts/settings.py', DIR_LOGGING + 'settings.py')


class PlaceLearning:
    def __init__(self):
	self.debug("INFO", "place_learning_node is running!")
        ####### publisher ##########
        self.goalMarkerPub = rospy.Publisher('/place_learning/goal_region', Marker, queue_size=10)
        self.goalMarkerPubTimer = time.time()


        ####### subscribers ########
        rospy.Subscriber("/fly_pose_in_arena", PoseStamped, self.flyPoseCB)
        self.flyPose = PoseStamped()

        ####### ACTION CLIENT ######
        self.move2poseClient = actionlib.SimpleActionClient('/mechanical_arena/move2pos', mm.move2poseAction)
        self.move2poseClient.wait_for_server()

        ####### CONTROL MODE CLIENT #####
        self.clientSetControlMode = rospy.ServiceProxy('/mechanical_arena/control_mode', set_mode)
        self.setGainsClient       = rospy.ServiceProxy('/mechanical_arena/set_gains', set_gains)

        rospy.loginfo('Waiting for mechanical arena')
        self.clientSetControlMode.wait_for_service()
        self.setGainsClient.wait_for_service()

        req = set_modeRequest()
        #req.state.data = 'velocity'
        req.state.data = 'open_loop'

        self.clientSetControlMode(req)


        self.laserTrig = laserTrigger(portCOM = portCOM)
        self.laserTrig.getInfo()
        self.laserTrig.start()

        self.listening = True
        threading.Thread(target=self.listenLaserTrigger).start()

        self.flyInGoal        = False

        self.inGoalTimer = time.time()



        ############## LOGGING CLIENTS ###############
        self.clientCameraViewDir = rospy.ServiceProxy('/imaging_view/set_data_directory', set_directory)
        self.clientRecordingView = rospy.ServiceProxy('/imaging_view/set_recording',      set_recording)
        self.clientArenaLogging  = rospy.ServiceProxy('/mechanical_arena/set_data_directory', set_log_directory)

        self.clientCameraViewDir.wait_for_service()
        self.clientRecordingView.wait_for_service()
        self.clientArenaLogging.wait_for_service()

        self.trial_counter = -1
        self.ix_exp = 0
        self.teaching_pos_counter = 0
        self.send_teaching_pos = False
        self.running = True

        self.state = 'RESSETING'
        self.reset2InitialPos()

        #self.setLogDirectories()





    def setLogDirectories(self):

        req = set_directoryRequest()
        req.dir = DIR_LOGGING + 'exp_{:d}/trial_{:d}'.format(self.ix_exp, self.trial_counter) + '/camera_view/'
        self.clientCameraViewDir(req)

        req = set_recordingRequest()
        req.action_type = "record"
        self.clientRecordingView(req)

        req = set_log_directoryRequest()
        req.dir       = DIR_LOGGING + 'exp_{:d}/trial_{:d}'.format(self.ix_exp, self.trial_counter) + '/mechanical_arena/'
        req.base_name = 'arena'
        self.clientArenaLogging(req)

    """ call backs functions """
    def flyPoseCB(self, data):
        self.flyPose = data



    def sendGoalPose(self, x_pos, y_pos, theta):
        theta = np.radians(theta)
        goalMsg = mm.move2poseGoal()
        goalMsg.goal_pose.position.x = x_pos
        goalMsg.goal_pose.position.y = y_pos
        goalMsg.velocity.linear.x = 15  # mm/s

        q = tf.transformations.quaternion_from_euler(0, 0, theta)
        goalMsg.goal_pose.orientation.x = q[0]
        goalMsg.goal_pose.orientation.y = q[1]
        goalMsg.goal_pose.orientation.z = q[2]
        goalMsg.goal_pose.orientation.w = q[3]

        self.move2poseClient.send_goal(goalMsg)


    def reset2InitialPos(self):

        lightState = False
        self.laserTrig.setTrigger([255, lightState])
        while self.laserTrig.triggerState[1]:
            self.laserTrig.mainLoop()
            time.sleep(0.01)
        time.sleep(0.1)




        req = set_modeRequest()
        req.state.data = 'open_loop'
        res = self.clientSetControlMode(req)
        time.sleep(0.5)

        th_pos = INITIAL_POSITIONS_TH[self.ix_exp]
        if th_pos == 'random':
            th_pos = np.random.choice([0, np.pi/2, np.pi, 3*np.pi/2])
        self.sendGoalPose(INITIAL_POSITIONS_X[self.ix_exp], INITIAL_POSITIONS_Y[self.ix_exp], th_pos)  # Send goal
        self.move2poseClient.wait_for_result()
        print('RESSETED POSITION!!')
        self.state = 'RUNNING'
        req = set_modeRequest()


        exp_running = True
        self.trial_counter += 1
        print("STARTING TRIAL ", self.trial_counter)

        if self.trial_counter >= TRIALS_PER_EXPERIMENT[self.ix_exp]:

            if WAIT_AT_THE_END_OF_EXPERIMENT[self.ix_exp] > 0:
                timerWait = time.time()
                while time.time() - timerWait < WAIT_AT_THE_END_OF_EXPERIMENT[self.ix_exp]:
                    print("WAITING")
                    time.sleep(1)

            self.ix_exp += 1
            print("STARTING EXPERIMENT ", self.ix_exp)


            self.trial_counter = 0

            if self.ix_exp >= NUM_EXPERIMENTS:
                self.running = False
                exp_running = False


        print(self.ix_exp)


        reqGains = set_gainsRequest()
        reqGains.gain_position = GAIN_TRANSLATION[self.ix_exp]
        reqGains.gain_rotation = GAIN_ROTATION[self.ix_exp]
        resGains = self.setGainsClient(reqGains)
        time.sleep(0.1)



        if exp_running:
            self.setLogDirectories()


        self.teaching_pos_counter = 0
        self.send_teaching_pos = True



        if exp_running:
            if not TEACHING[self.ix_exp]:
                req.state.data = 'closed_loop'
                res = self.clientSetControlMode(req)
                self.flyInGoal = False
                time.sleep(0.5)
                self.trialTimer = time.time()
            else:
                req.state.data = 'open_loop'
                res = self.clientSetControlMode(req)
                self.flyInGoal = False
                time.sleep(0.5)
                self.trialTimer = time.time()


    """ Code for the main thread of the node """
    def mainThread(self):
        if not self.running:
            print('FINISHED!')
            time.sleep(5)
            return
        if self.running:
            self.publishGoalMarker()



        if self.state == 'RUNNING':

            fly_pos = [self.flyPose.pose.position.x, self.flyPose.pose.position.y]
            fly_pos = np.array(fly_pos) * 1000  # to mm

            quaternion = (
                self.flyPose.pose.orientation.x,
                self.flyPose.pose.orientation.y,
                self.flyPose.pose.orientation.z,
                self.flyPose.pose.orientation.w)

            euler = tf.transformations.euler_from_quaternion(quaternion)
            if TEACHING[self.ix_exp]:
                if self.teaching_pos_counter < len(TEACHING_POS_X[self.ix_exp]):
                    if self.send_teaching_pos:
                        print(TEACHING_POS_X[self.ix_exp][self.teaching_pos_counter], TEACHING_POS_Y[self.ix_exp][self.teaching_pos_counter], TEACHING_POS_TH[self.ix_exp][self.teaching_pos_counter])
                        self.sendGoalPose(TEACHING_POS_X[self.ix_exp][self.teaching_pos_counter],
                                          TEACHING_POS_Y[self.ix_exp][self.teaching_pos_counter],
                                          TEACHING_POS_TH[self.ix_exp][self.teaching_pos_counter])  # Send goal
                        self.send_teaching_pos = False


                    dist_pos = fly_pos - np.array([TEACHING_POS_X[self.ix_exp][self.teaching_pos_counter], TEACHING_POS_Y[self.ix_exp][self.teaching_pos_counter]])
                    dist_pos = np.sqrt(np.power(dist_pos, 2).sum())
                    dist_th = np.abs(TEACHING_POS_TH[self.ix_exp][self.teaching_pos_counter] - np.degrees( euler[2]) )
                    dist_th = dist_th % 360


                    if dist_pos < 1:
                        if dist_th > 360-10 or dist_th < 10:
                            self.send_teaching_pos = True
                            self.teaching_pos_counter += 1


            #print(euler)


            laser_power_base = 0
            if LASER_ON[self.ix_exp]:
                laser_power_base = 125


            r_fly = np.sqrt(fly_pos[0]**2 + fly_pos[1]**2)
            laser_power = laser_power_base# int(np.clip(laser_power_base + (r_fly/INCREASE_LASER_POWER_RADIUS)*(255 - laser_power_base), 0, 255))

            #if fly_pos[0]**2 + fly_pos[1]**2 > INCREASE_LASER_POWER_RADIUS**2:
            #    laser_power = 255

            if fly_pos[0] >= GOAL_POSITIONS_X[self.ix_exp] - RADIUS_GOAL[self.ix_exp] and fly_pos[0] <= GOAL_POSITIONS_X[self.ix_exp] + RADIUS_GOAL[self.ix_exp] and fly_pos[1] >= GOAL_POSITIONS_Y[self.ix_exp] - RADIUS_GOAL[self.ix_exp] and fly_pos[1] <= GOAL_POSITIONS_Y[self.ix_exp] + RADIUS_GOAL[self.ix_exp]:
                if not self.flyInGoal:
                    self.inGoalTimer = time.time()
                    self.flyInGoal   = True

                laser_power = 0

                delayedInGoal = time.time() - self.inGoalTimer
                if delayedInGoal > TIME_IN_GOAL_TO_FINISH:
                    self.state = 'RESSETING'




            else:
                self.flyInGoal = False

            if time.time() - self.trialTimer > MAX_TIME_PER_TRIAL[self.ix_exp]:
                self.state = 'RESSETING'
                print("TIME OUT FOR TRIAL!")

            self.laserTrig.setTrigger([255 - laser_power, 255*LIGHT_ON[self.ix_exp]])
            self.laserTrig.mainLoop()


        elif self.state == 'RESSETING':
            self.reset2InitialPos()






    def publishGoalMarker(self):
        if time.time() - self.goalMarkerPubTimer > 1.0/PUBLISH_MARKER_FREQ:
            marker = Marker()
            marker.header.frame_id = "/base_link"
            marker.type = marker.CUBE
            marker.action = marker.ADD

            marker.scale.x = float(2*RADIUS_GOAL[self.ix_exp])/1000
            marker.scale.y = float(2*RADIUS_GOAL[self.ix_exp])/1000
            marker.scale.z = 0.001

            marker.color.a = 0.7
            marker.color.r = 0
            marker.color.g = 1
            marker.color.b = 0
            marker.pose.orientation.x = 0
            marker.pose.orientation.y = 0
            marker.pose.orientation.z = 0
            marker.pose.orientation.w = 1
            marker.pose.position.x = float(GOAL_POSITIONS_X[self.ix_exp])/1000
            marker.pose.position.y = float(GOAL_POSITIONS_Y[self.ix_exp])/1000
            marker.pose.position.z = 0
            marker.lifetime = rospy.Duration(10)
            self.goalMarkerPub.publish(marker)

            self.goalMarkerPubTimer = time.time()

    def listenLaserTrigger(self):
        while self.listening:
            self.laserTrig.listenArduino()
            time.sleep(1./(ROS_NODE_FREQ*2))

    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('place_learning_node', anonymous=True)
        rate = rospy.Rate(ROS_NODE_FREQ)    # Hz
        node = PlaceLearning()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

