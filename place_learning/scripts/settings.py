import numpy as np

ARD_BAUD_RATE = 57600
portCOM = '/dev/ttyUSB0'
SEND_DATA_FREQ   = 20         # Hz
RECEIVE_DATA_FREQ = 10         # Hz
ROS_NODE_FREQ     = 100
PUBLISH_MARKER_FREQ = 1 # Hz
TIME_NOT_RECEIVING_MSGS = 2 # sec


TIME_IN_GOAL_TO_FINISH = 30 # sec



NUM_EXPERIMENTS = 2

TRIALS_PER_EXPERIMENT = [1,1]#[1, 10, 1]
LASER_ON              = [True, True]#[False, True, False]


INITIAL_POSITIONS_X = [0,0]#[30, 30, 30]  # mm
INITIAL_POSITIONS_Y = [0,0]#[0, 0, 0]  # mm
INITIAL_POSITIONS_TH = [0,0]#[0, 0, 0]

GOAL_POSITIONS_Y = [-100, -100]#[-100,0, -100]  # mm #[-100,-50, -100]  # mm
GOAL_POSITIONS_X = [0,0]#[0, -50, 0]  # mm #[0, 0, 0]  # mm




TEACHING = [False, False, False]#[False, False, False, False, False]
TEACHING_POS_X  = [None]*NUM_EXPERIMENTS#[None, [0,-60], None, None, None]
TEACHING_POS_Y  = [None]*NUM_EXPERIMENTS#[None, [0, 0], None, None, None]
TEACHING_POS_TH = [None]*NUM_EXPERIMENTS#[None, [90, 90], None, None, None]

WAIT_AT_THE_END_OF_EXPERIMENT = [0,0,0]#[0, 0, 30*60, 0, 0] #sec

MAX_TIME_PER_TRIAL = [60*15, 60*15]#[300, 300, 300] # sec


RADIUS_GOAL     = [1,1]#[1, 25, 1]  # mm
LIGHT_ON        = [True, True, True]

GAIN_ROTATION    = [1,1,1]
GAIN_TRANSLATION = [0,0.2]#[0.2,0.2,0.2]

INCREASE_LASER_POWER_RADIUS  = 40       # mm. The width of the cylindrical wall in the arena. The width of the cylindrical wall in the arena