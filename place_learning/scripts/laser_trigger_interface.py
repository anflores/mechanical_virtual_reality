import serial
import time
import numpy as np
from settings import *


try:
    import rospy
except:
    pass

def rosLogger(typ, info):
    if typ == 'WARNINIG':
        rospy.logwarn(info)
    elif typ == 'ERROR':
        rospy.logerr(info)
    elif typ == 'INFO':
        rospy.loginfo(info)


def defaultLogger(typ, info):
    print(typ + ": " + info)




class laserTrigger:
    def __init__(self, portCOM, logger = defaultLogger):
        self.ard = serial.Serial(port= portCOM,
                                 baudrate=ARD_BAUD_RATE,
                                 parity=serial.PARITY_NONE,
                                 stopbits=serial.STOPBITS_ONE,
                                 bytesize=serial.EIGHTBITS,
                                 )
        self.logger = logger


        time.sleep(2)
        self.started = False
        self.listening = False


    def getInfo(self):
        self.ard.flushInput()
        self.ard.flushOutput()

        self.ard.write('info\n')
        time.sleep(0.5)
        r = self.ard.readline().split(',')
        if r == '':
            self.logger("ERROR", "NOT RECEIVED INFO FROM ARDUINO!")

        self.name = r[0]
        self.numTriggers = int(r[1])
        self.nameTriggers = []
        for i in range(self.numTriggers-1):
            self.nameTriggers.append(r[2+i])


        self.nameTriggers.append(r[2+self.numTriggers-1].split('\r')[0])

        self.logger("INFO", " " * 5 + "Name micromanipulator: " + self.name)
        self.logger("INFO", " " * 5 + "Number of triggers:    " + str(self.numTriggers))
        self.logger("INFO", " " * 5 + "Name of the motors:    " + str(self.nameTriggers) + "\n")


    def start(self):
        self.ard.write('start {:d}\n'.format(RECEIVE_DATA_FREQ))   # Send arduino command_line to send motors state
        self.triggerState = np.zeros(self.numTriggers)
        self.setTriggerMsg= np.zeros(self.numTriggers)
        self.timer_reciv_data = time.time()
        self.sendTimer = time.time()
        self.started = True



    def listenArduino(self):
        cb = self.ard.readline()
        if cb != '':
            try:
                cb = cb.split(',')
                if len(cb) == self.numTriggers:
                    for i in range(self.numTriggers):
                        self.triggerState[i] = int(cb[i])
                        self.timer_reciv_data = time.time()
                        self.ard.flushInput()
                else:
                    self.ard.flushInput()
            except:
                self.logger("WARNING", "ARDUINO DATA CANNOT BE READ FROM:" + str(self.name))
                pass



    def setTrigger(self, triggerState):
        self.setTriggerMsg = triggerState


    def sendTriggerOutput(self):
        cmd = ""
        for i in range(self.numTriggers):
            cmd += str(int(self.setTriggerMsg[i])) + ","
        cmd = cmd[:-1] + "\n"
        self.ard.write(cmd)



    def mainLoop(self):
        if not self.started:
            return

        if (time.time() - self.sendTimer) > 1.0/SEND_DATA_FREQ:
            self.sendTriggerOutput()
            self.sendTimer = time.time()

        if time.time() - self.timer_reciv_data > TIME_NOT_RECEIVING_MSGS:
            self.logger('ERROR', '{:s} does not receive arduino data!'.format(self.name))





if __name__ == '__main__':
    trig = laserTrigger(portCOM = portCOM)
    trig.getInfo()
    trig.start()
    while True:
        trig.mainLoop()
