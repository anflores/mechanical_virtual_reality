#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import rospkg
from settings import *
from camera_frame import cameraViews
from recorder import VideoRecorder
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import os
from camera_view.srv import set_directory, set_directoryResponse, set_directoryRequest
from camera_view.srv import set_recording, set_recordingResponse, set_recordingRequest
import time

class CameraView:
    def __init__(self):
        self.debug("INFO", "camera view node is running!")

        rospack = rospkg.RosPack()
        configDir = rospack.get_path('camera_view') + '/' + CAMERA_CONFIG_DIR + '/'

        rospy.loginfo("Camera view waiting for cameras")
        self.view = cameraViews(configDir)
        rospy.loginfo("Cameras found!")
        self.recorder = None
        self.isRecording = True
        self.stopRecording = True


        ####### publisher ##########
        #self.viewPub = rospy.Publisher('camera_view', Image, queue_size=10)

        ####### subscribers ########
        #rospy.Subscriber("chatter", String, self.callback)
        self.subs = ""

        self.setDataDirServer = rospy.Service('/imaging_view/set_data_directory', set_directory, self.getDataDirCB)
        self.setRecordServer  = rospy.Service('/imaging_view/set_recording', set_recording, self.setRecordingCB)

        self.setContinuousRecording = False
        self.bridge = CvBridge()
        cv2.namedWindow("Experiment view")

    """ call backs functions """

    def getDataDirCB(self, req):
        tmp_isrecording = self.isRecording
        self.isRecording = False

        time.sleep(0.5)

        if self.recorder is not None:
            self.recorder.endFile()
            self.recorder.close()

        if not os.path.exists(req.dir):
            os.makedirs(req.dir)

        self.recorder = VideoRecorder(req.dir, 'view')
        self.isRecording = tmp_isrecording

        res = set_directoryResponse()
        res.completed = True

        return res


    def setRecordingCB(self, req):
        res = set_recordingResponse()

        if req.action_type == 'record':
            self.isRecording = True
            rospy.loginfo('Camera view is being recorded!')
            res.state = 'recording'

        else:
            if self.isRecording:
                self.stopRecording = True
            rospy.loginfo('Camera view is NOT being recorded!')
            res.state = 'stopped'

        return res


    """ Code for the main thread of the node """
    def mainThread(self):
        frame = self.view.mainLoop()
        stamp = rospy.Time.now().to_nsec()

        if self.recorder is not None:
            if self.isRecording:
                self.recorder.add_frame(frame, stamp)


                if self.stopRecording:
                    self.isRecording = False
                    self.recorder.endFile()
                    self.stopRecording = False




        cv2.waitKey(1)

        cv2.imshow("Experiment view", frame)

        self.pub = ""



    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('camera_view_node', anonymous=True)
        rate = rospy.Rate(ROS_RATE)    # 10 Hz
        node = CameraView()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()
        node.recorder.close()

    except rospy.ROSInterruptException:
        try:
            node.recorder.close()
        except:
            pass

