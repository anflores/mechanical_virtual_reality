import pypylon.pylon as pylon
import numpy as np
import ntpath

class Camera(object):
    """
    Class to run pylon wrapper, acquire mages and so on...
    """
    def __init__(self, pfs_path):
        """
        Opening camera
        """
        tlfactory = pylon.TlFactory.GetInstance()
        devices = tlfactory.EnumerateDevices()
        self.camera = None
        self.pFormatConverter = pylon.ImageFormatConverter()
        sn = ntpath.split(pfs_path)[-1].split('.')[0]

        for d in devices:
            if sn in d.GetFriendlyName():
                self.camera = pylon.InstantCamera(tlfactory.CreateDevice(d))
                self.name = d.GetFriendlyName()
                self.sn = sn

        if self.camera is not None:
            print("Found camera", self.name)
            with open(pfs_path, "r") as pfs:
                settings = pfs.read()
            self.camera.Open()
            pylon.FeaturePersistence.LoadFromString(settings, self.camera.GetNodeMap())
            self.camera.MaxNumBuffer = 10



    def start(self):
        if self.camera is not None:
            self.camera.Open()
            #self.camera.StartGrabbing(pylon.GrabStrategy_OneByOne)
            self.camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly)

            self.grab_result = pylon.GrabResult()
            return True
        else:
            return False


    def grab_frame(self):
        try:
            self.grab_result = self.camera.RetrieveResult(15)
            if self.grab_result.GrabSucceeded():
                data = np.array(self.grab_result.GetBuffer()).astype(np.uint8)
                image = data.reshape(self.grab_result.GetHeight(), self.grab_result.GetWidth())
                return image, self.grab_result.TimeStamp
            else:
                return None, None
        except:
            return None, None

    def close(self):
        self.camera.Close()