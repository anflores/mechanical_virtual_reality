import time
import numpy as np
import cv2
from camera import Camera
from settings import *


FONT = cv2.FONT_HERSHEY_SIMPLEX
FONTSCALE = 1


class cameraViews:
    def __init__(self, CONFIG_DIR):
        self.frontCamera  = Camera(CONFIG_DIR + FRONT_CAMERA)
        self.frontCamera.start()


        self.frontFrame = np.zeros([480, 640], dtype = 'uint8')


    def mainLoop(self):


        frontFrame , _ = self.frontCamera.grab_frame()


        if frontFrame is not None:
            self.frontFrame = np.array(frontFrame, dtype = 'uint8')

        return self.frontFrame





if __name__ == '__main__':
    CAMERA_DIR = '/home/nci_la/nci_lab/src/camera_view/camera_config/'
    cameraviews = cameraViews(CAMERA_DIR)

    cv2.namedWindow("Experiment view")

    print('press q to quit')
    while cv2.waitKey(1) != ord('q'):
        frame = cameraviews.mainLoop()
        cv2.imshow("Experiment view", frame)

