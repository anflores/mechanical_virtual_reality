import numpy as np
import os
from time import localtime, strftime
from datetime import datetime
import cv2

from settings import *


class VideoRecorder(object):
    """
    Class to record video stream and so on...
    """



    def __init__(self, base_path, base_name):
        self.base_path = base_path
        self.base_name = base_name
        self.file_counter = 0
        self.frame_counter = 0
        self.fourcc = cv2.VideoWriter_fourcc(*'MJPG')  # cv2.VideoWriter_fourcc() does not exist

        print 'Set up recording', self.base_path, self.base_name


    def add_frame(self, frame, rosStamp):
        if self.frame_counter == 0:
            self.startFile(frame)

        d = np.stack([frame, frame, frame], 2)      #grayscale video not supported, you get a broken file instead
        self.video_writer.write(d)
        line = str(self.frame_counter) + ', ' + str(rosStamp) + '\n'
        self.stampfile.write(line)
        self.frame_counter += 1



        if self.frame_counter == MAX_FRAMES_PER_FILE:
            self.endFile()

    def endFile(self):
        self.video_writer.release()
        self.stampfile.close()
        self.frame_counter = 0
        self.file_counter += 1


    def startFile(self, frame):
        new_filename = os.path.join(self.base_path, self.base_name) + '_' + str(self.file_counter)
        self.video_writer = cv2.VideoWriter(
            new_filename + '.avi',
            self.fourcc,
            ROS_RATE,  # frames per second
            tuple(reversed(frame.shape)),
            isColor=True  # grayscale video not supported
        )
        self.stampfile = open(new_filename + '.txt', 'w')


    def close(self):
        print 'Close recording', self.base_name
        self.video_writer.release()


    def __repr__(self):
        return "Recorder for %s" % str(self.base_name)