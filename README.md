# Mechanical Virtual Reality
<img src="images/mechanical_vr.png"  width="1000" height="500">

This repository contains the design files to build the mechanical virtual reality and the code to run it, described in [1].




# Build
<img src="images/rotation_and_translation.png"  width="1000" height="500">

The mechanical virtual reality is a costum build from typical mechanical parts from 3D printers and aluminium machined parts. These aluminium parts could be in theory replaced by 3D printed materials, such as PLA.

The whole build of the mechanical virtual reality is provided in this [step file](build/Fly Arena 2020-003-001.stp)  or in this [stl file](build/Fly_arena_2020-003-001.stl)

The setup is basically composed by 2 components: a rotation component, which rotates the whole arena around the fly and allows continuous rotations using a slip ring, and a translational frame to offset the arena around the fly. 


## Rotational component
The following list includes all the machined parts for the rotation component

| Model | Quantity | 
| ---      |  ---- |
|[support slip ring](build/rotation_component_parts/support_slip_ring.stl) | 1 |
|[support gear slip ring](build/rotation_component_parts/support_gear_slip_ring.stl) | 1 |
|[gear slip ring](build/rotation_component_parts/gear_slip_ring.stl) | 1 |
|[support bldc motor](build/rotation_component_parts/support_bldc_motor.stl) | 1 |
|[support bldc motor top](build/rotation_component_parts/support_bldc_motor_top.stl) | 1 |


The rest of the components can be bought. We provide a link for a buying option:

| Component | Quantity | link to buy | 
| ---      |  ---- | ------  |
| Slip ring H90185-0310-09S| 1 | https://www.senring.com/through-hole-slip-ring/medium-hole/h90185.html |
| Odrive V3.6 | 1 | https://odriverobotics.com/shop/odrive-v36 |
| Brushless motor | 1 | https://odriverobotics.com/shop/odrive-custom-motor-d5065 |
| Encoder motor   | 1 | https://odriverobotics.com/shop/cui-amt-102 |
| Motor enclosure | 1 | https://discourse.odriverobotics.com/t/nema-enclosures-for-d5065-and-d6374-motors/830 |
| Gear motor   | 1 |  missing link   |

The gear for the brushless motor needs to be modified for the shaft.


## Translational component
The following list includes all the machined parts for the translation component

| Model | Quantity | 
| ---      |  ---- |
|[support translational stage](build/translation_component_parts/support_translational_stage.stl) | 1 |
|[translation x corner 1](build/translation_component_parts/translation_x_corner_1.stl) | 1 |
|[translation x corner 2](build/translation_component_parts/translation_x_corner_2.stl) | 1 |
|[translation x corner 3](build/translation_component_parts/translation_x_corner_3.stl) | 1 |
|[translation x corner 4](build/translation_component_parts/translation_x_corner_4.stl) | 1 |
|[belt clamp x](build/translation_component_parts/belt_clamp_x.stl) | 1 |
|[belt clamp y](build/translation_component_parts/belt_clamp_y.stl) | 1 |
|[arena](build/translation_component_parts/arena.stl) | 1 |

And these are the components to buy:

| Component | Quantity | link to buy | 
| ---      |  ---- | ------  |
| CNC shield | 1 | https://www.conrad.de/de/p/joy-it-ard-cnc-kit1-motor-shield-1646889.html|
| Nema 17 stepper motor | 2 | https://www.conrad.de/de/p/joy-it-schrittmotor-nema-17-01-nema-17-01-0-4-nm-1-68-a-wellen-durchmesser-5-mm-1597325.html|
| TMC 2130 driver | 2 | https://www.i-love-tec.de/Technik-von-Hilitand/Technik-zu-tmc2130|
| Gt2 Pulley 20 teeth, 5mm Bore | 4 | https://www.amazon.de/Pulley-Timing-Printer-Accessories-Included/dp/B07K79D985/ref=sr_1_2_sspa?dchild=1&keywords=gt2+20+z%C3%A4hne+5+mm+bohrung&qid=1620403218&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFNSDBIN0xKNDBNWTYmZW5jcnlwdGVkSWQ9QTA4ODQ2MjQzVEdBMjM3MjdYWDVFJmVuY3J5cHRlZEFkSWQ9QTA3ODc5MjQyOFJJQlI4OEVYVUhSJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ== |
| Gt2 Timing belt | 1 | https://www.amazon.com/BEMONOC-5Meters-T2-5-Timing-Trapezoidal/dp/B017U0F6YM/ref=sr_1_4?crid=1UOKC8O3WK4WA&keywords=timing+belt+t2&qid=1677839719&sprefix=timing+belt+t%2Caps%2C180&sr=8-4|
| Arduino Due | 1 | https://eu.robotshop.com/products/arduino-due-32bit-arm-microcontroller?gclid=Cj0KCQiA0oagBhDHARIsAI-BbgcW1iG652YrY47pbm8B7MMd9-7XKoO1xDazWoGUw-U7AWyN2Iz7beAaAsxFEALw_wcB |



# Electronics

You need to make the following connections. For the brushless motor, you can connected following this guide:

https://docs.odriverobotics.com/v/0.5.4/getting-started.html#hardware-requirements


For the translational part, you will need to do some soldering to bridge the CNC shield signal cables through the sleep ring, as shown in this figure:

<img src="images/electronics.png"  width="1000" height="500">



# Software requirements
## ROS

A dedicated desktop computer running Ubuntu 18.04LTS or 19.04LTS is required for this software. Newer versions of Ubuntu will not work, because this project works only with ROS melodic (using python 2.7), and not ROS noetic (which uses python 3.4). This will be fixed in future versions.

The software runs under the framework of ROS (Robot Operating System, https://www.ros.org/). This repository contains ROS packages to control the motorized micromanipulators required for laser surgery. First, install ros melodic:
http://wiki.ros.org/melodic/Installation/Ubuntu


then create a catkin workspace:
http://wiki.ros.org/catkin/Tutorials/create_a_workspace


After this, open a terminal and navigate to your source folder in your catking workspace:

> cd ~/catkin_ws/src/

and clone this project:

> git clone https://gitlab.com/anflores/mechanical_virtual_reality

Finally, build the project:

> cd ~/catkin_ws

> catkin_make


### Odrive
Odrive controls the brushless motor for the rotational component. You will need to configure odrive according to your brushless motor, you can use the odrivetool for this:
https://docs.odriverobotics.com/v/devel/odrivetool.html




### Arduino
The stepper motors from the translational component are controlled with an Arduino DUE, which receives velocity commands from the computer, therefore you need to upload the necessary code to the arduinos. 
First download and install the arduino IDE https://www.arduino.cc/en/software
After the arduino IDE is installed, you need to copy the [libraries](mechanical_arena_driver/Arduino/libraries/) in your arduino library folder.


Then you need to upload the script [/mechanical_arena_driver/Arduino/micromanipulator_driver/micromanipulator_driver.ino](/mechanical_arena_driver/Arduino/micromanipulator_driver/micromanipulator_driver.ino) to the Arduino Due.


### Ball tracking
 The mechanical arena is controlled in closed loop with the activity of the fly walking on a ball. The walking activity is monitored with a ball tracking algorithm, that you also need to setup:

 https://gitlab.com/anflores/ball_tracking
 



# Usage
Make sure that everything is connected: The Odrive, the arduino Due and the camera for ball tracking. Then you can run

> roslaunch mechanical_description stripes_arena.launch

If everything is fine, 3 windows should show: one with rviz, which shows the fly on the virtual mechanical arena, and another one that shows the control mode of the arena.



## Questions
If you have any questions or problems, send us an e-mail or open a ticket in this repository. Give us feedback if something is not clear, and thanks for using the mechanical virtual reality!


## References
<a id="1">[1]</a> [1] Flores-Valle, A., & Seelig, J. D. (2022). A place learning assay for tethered walking Drosophila. Journal of Neuroscience Methods, 378, 109657.
