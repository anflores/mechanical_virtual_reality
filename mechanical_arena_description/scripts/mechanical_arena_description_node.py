#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PolygonStamped, Polygon, Point32
from arena_collision import *
from settings import *
from mechanical_vr_driver.srv import get_collision, get_collisionResponse


class MechanicalArenaDescription:
    def __init__(self):
	self.debug("INFO", "mechanical_arena_description_node is running!")
        ####### publisher ##########
        self.VisualCollisionLimPub = rospy.Publisher('mechanical_arena/visualization/collision_polygon', PolygonStamped, queue_size=10)

        ####### subscribers ########
        self.subs = ""

        self.wall = loadColissionWall('/home/nci_la/nci_lab/src/mechanical_arena_description/arena_collisions/arena_maze.txt')
        self.wall_dilated = dilateWall(self.wall, SAFE_DISTANCE_FROM_WALL)
        self.wall_dilated = np.true_divide(self.wall_dilated, 1000) # Convert to meters

        self.polyWall =  Polygon()
        for i in range(len(self.wall_dilated)):
            p = Point32()
            p.x = self.wall_dilated[i,0]
            p.y = self.wall_dilated[i,1]
            p.z = 0
            self.polyWall.points.append(p)

        self.collisionService = rospy.Service('mechanical_arena/collision', get_collision, self.sendCollisionCB)

    """ call backs functions """
    def sendCollisionCB(self, data):
        msg = get_collisionResponse()
        msg.collision = self.polyWall
        return msg



    """ Code for the main thread of the node """
    def mainThread(self):
        self.publishVisualCollisionLimit()
        self.pub = ""

    def publishVisualCollisionLimit(self):


        polyStamp = PolygonStamped()
        polyStamp.header.stamp = rospy.Time.now()
        polyStamp.polygon = self.polyWall
        polyStamp.header.frame_id = 'base_link'

        self.VisualCollisionLimPub.publish(polyStamp)


    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('mechanical_arena_description_node', anonymous=True)
        rate = rospy.Rate(1)    # 10 Hz
        node = MechanicalArenaDescription()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass

