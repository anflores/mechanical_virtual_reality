from shapely.geometry import LineString
import numpy as np


def getLineString(wall):
    wall_ = np.zeros([wall.shape[0] + 1, wall.shape[1]])
    wall_[:-1, :] = wall
    wall_[-1, :] = wall[0, :]
    return LineString(wall_)


def extract_poly_coords(geom):
    if geom.type == 'Polygon':
        exterior_coords = geom.exterior.coords[:]
        interior_coords = []
        for interior in geom.interiors:
            interior_coords += interior.coords[:]
    elif geom.type == 'MultiPolygon':
        exterior_coords = []
        interior_coords = []
        for part in geom:
            epc = extract_poly_coords(part)  # Recursive call
            exterior_coords += epc['exterior_coords']
            interior_coords += epc['interior_coords']
    else:
        raise ValueError('Unhandled geometry type: ' + repr(geom.type))
    return {'exterior_coords': exterior_coords,
            'interior_coords': interior_coords}


def dilateWall(wall, dilate_distance, cap_style=1, join_style=3):
    line = getLineString(wall)
    dilated = line.buffer(dilate_distance, cap_style=cap_style, join_style=join_style)
    wall_dilated = extract_poly_coords(dilated)
    wall_dilated = np.array(wall_dilated['interior_coords'])
    wall_dilated = wall_dilated[:-1]  # Take out the last point, since it is the same as the first one
    return np.flip(wall_dilated, axis=0)  # Reverse order


def loadColissionWall(DIR_COLLISSION):
    arena_vertices = open(DIR_COLLISSION, 'r')
    x = []
    y = []

    for line in arena_vertices.readlines():
        x_co, y_co = line.split(' ')
        x.append(float(x_co))
        y.append(float(y_co))

    wall = np.stack([x, y]).T
    return wall
