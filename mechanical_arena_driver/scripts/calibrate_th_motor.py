#!/usr/bin/env python
import rospy
from std_msgs.msg import String


import pygame
from thMotor_interface import thMotorInterfaceAPI
from xyMotor_interface import xyMotors
from settings import *
import numpy as np
import time

if __name__ == '__main__':
    rospy.init_node('mechanical_vr_driver_node', anonymous=True)
    rate = rospy.Rate(100)    # 10 Hz

    pygame.init()



    thDriver = thMotorInterfaceAPI()
    thDriver.connect()

    #thDriver.calibrate()  # does a calibration
    rospy.loginfo("Saved configuration!")
    time.sleep(2)

    thDriver.setControlGains(POS_GAIN_TH_MOTOR_VELOCITY_CONTROL, VEL_GAIN_TH_MOTOR_VELOCITY_CONTROL, VEL_INTEGRATOR_GAIN_TH_MOTOR_VELOCITY_CONTROL)
    thDriver.engage()  # get ready to drive





    pygame.font.init()  # you have to call this at the start,
    textFont = pygame.font.SysFont('Comic Sans MS', 30)
    screen = pygame.display.set_mode((640, 480))
    screen.fill((0, 0, 0))
    text = []
    text.append(textFont.render('Use the keys "q" and "e" to move the TH motor', True, (255, 255, 255)) )

    for i in range(len(text)):
        screen.blit(text[i], (100, 200 + i*30))
    pygame.display.update()




    pressed_turn_left = False
    pressed_turn_right = False

    while not rospy.is_shutdown():
        # event loop
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:  # check for key presses
                if event.key == pygame.K_q:  # left arrow turns left
                    pressed_turn_left = True
                elif event.key == pygame.K_e:  # right arrow turns right
                    pressed_turn_right = True



            elif event.type == pygame.KEYUP:  # check for key releases
                if event.key == pygame.K_q:  # left arrow turns left
                    pressed_turn_left = False
                elif event.key == pygame.K_e:  # right arrow turns right
                    pressed_turn_right = False


        velth = 3 # Turns/sec

        vel_th = 0

        if pressed_turn_left:
            vel_th = velth
        if pressed_turn_right:
            vel_th = -velth

        thDriver.set_velocity(vel_th)

        rate.sleep()



