#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from thMotor_interface import thMotorInterfaceAPI
from xyMotor_interface import xyMotors
from geometry_msgs.msg import TwistStamped, Twist, Pose, Polygon, PointStamped, PoseStamped, PolygonStamped, Point32
from collision import *

from mechanical_arena_driver.srv import set_mode, set_modeRequest, set_modeResponse
from mechanical_arena_driver.srv import set_gains, set_gainsRequest, set_gainsResponse

from mechanical_arena_driver.srv import set_log_directory, set_log_directoryRequest, set_log_directoryResponse
import mechanical_arena_driver.msg as mm

from log import dataFile
import tf
from tf import transformations
import numpy as np
import math
from settings import *
import time
import datetime
import os
import Tkinter
from gui_control import GUIcontrol, BUTTON_INTERSPACE_X
import actionlib
from pathPlanner import PathPlanner

def limitValue(value, max_lim=360):
    v = value % max_lim
    return v


class MechanicalVrDriver:
    def __init__(self, arena_collision = None):
        self.root = Tkinter.Tk()
        self.gui_control = GUIcontrol(self.root)
        self.root.wm_title("Mechanical Arena GUI Control")
        self.root.geometry('{:d}x{:d}'.format(BUTTON_INTERSPACE_X * 4, 100))
        self.df = None
        self.loggingData = False
        self.control_mode = 'closed_loop'

        self.manualVel = TwistStamped()
        self.ballDispCmd = Twist()
        self.dispCmd     = Twist()
        self.prevDispCmd = Twist()
        self.receivedDisp = False

        self.started = False
        self.debug("INFO", "mechanical_vr_driver_node is running!")
        self.xyDriver = xyMotors("mechanical_arena_xy")
        self.xyDriver.connect()
        self.xyDriver.start()

        self.thDriver = thMotorInterfaceAPI()
        self.thDriver.connect()
        # self.thDriver.engage()  # get ready to drive

        self.thDriver.setPositionControl()
        self.thDriver.setControlGains(POS_GAIN_TH_MOTOR_POSITION_CONTROL, VEL_GAIN_TH_MOTOR_POSITION_CONTROL,
                                      VEL_INTEGRATOR_GAIN_TH_MOTOR_POSITION_CONTROL)


        self.GAIN_TRANSLATION = GAIN_TRANSLATION
        self.GAIN_ROTATION    = GAIN_ROTATION

        ####### publisher ##########
        self.flyPosPub = rospy.Publisher('/fly_pose_in_arena', PoseStamped, queue_size=10)
        self.collisionPub = rospy.Publisher('/collision_arena', PolygonStamped, queue_size=10)

        self.flyPosPubTimer = time.time()

        ####### subscribers ########
        rospy.Subscriber("/ball_tracking/displacement", Twist, self.DisplacementCmdCB)
        rospy.Subscriber("/manual_control/cmd_vel", TwistStamped, self.manualVelCmdCB)

        rospy.Subscriber("/ball_tracking/tracking_data", PointStamped, self.trackingDataCB)
        self.ball_tracking_data = PointStamped()

        ########### SERVICES #############
        self.setDataDirServer = rospy.Service('/mechanical_arena/set_data_directory', set_log_directory,
                                              self.getDataDirCB)
        self.setControlMode = rospy.Service('/mechanical_arena/control_mode', set_mode, self.setControlModeCB)
        self.setGainsServer = rospy.Service('/mechanical_arena/set_gains', set_gains, self.setGainsCB)

        ####### ACTION SERVER  ##########
        self.move2poseActionServer = actionlib.SimpleActionServer('/mechanical_arena/move2pos', mm.move2poseAction,
                                                                  execute_cb=self.planPathCB, auto_start=False)
        self.move2poseActionServer.start()

        self.set_xyvel = [0, 0]
        self.set_pv_velxy = [0, 0]
        self.set_thvel = 0
        self.set_prev_thvel = 0

        self.timer = time.time()

        self.filter_xy = 0.02
        self.filter_th = 0.02

        self.timer = time.time()

        self.xMotorVel_acc = []
        self.yMotorVel_acc = []
        self.thMotorVel_acc = []

        self.ave_time = 0.1  # sec

        self.set_logger = False

        rospy.loginfo("ARENA IS READY!")
        self.started = True

        self.tfBr = tf.TransformBroadcaster()

        rospy.loginfo('MECHANICAL ARENA IS RUNNING!')


        self.posth_offset_ball  = 0
        self.posx_offset_ball = 0
        self.posy_offset_ball = 0
        self.posth_motor_command = 0


        self.collision = Collision(arena_collision)
        self.pathplanner = None
        if arena_collision is not None:
            self.pathplanner = PathPlanner(arena_collision)
            self.pathplanner.buildWeightMap(radius_arena=DIAMETER_ARENA / 2, min_dist=-2)

        self.collisionPubTimer = time.time()
        self.setHome()


    """ call backs functions """

    def setControlModeCB(self, req):
        if req.state.data == 'open_loop':
            rospy.logerr('MECHANICAL_ARENA: changed to velocity control')

            self.control_mode = 'open_loop'
            self.thDriver.setControlGains(POS_GAIN_TH_MOTOR_VELOCITY_CONTROL, VEL_GAIN_TH_MOTOR_VELOCITY_CONTROL,
                                          VEL_INTEGRATOR_GAIN_TH_MOTOR_VELOCITY_CONTROL)
            self.thDriver.setVelocityControl()
            self.thDriver.set_velocity(0)

            rospy.loginfo('MECHANICAL_ARENA: changed to velocity control')
        elif req.state.data == 'closed_loop':
            #self.thDriver.set_velocity(0)
            self.control_mode = 'closed_loop'

            self.thDriver.setControlGains(POS_GAIN_TH_MOTOR_POSITION_CONTROL, VEL_GAIN_TH_MOTOR_POSITION_CONTROL, VEL_INTEGRATOR_GAIN_TH_MOTOR_POSITION_CONTROL)
            self.thDriver.setPositionControl()
            self.thDriver.set_pos(self.thDriver.get_pos())

        else:
            rospy.logwarn('MECHANICAL_ARENA: control mode {:s} does not exist, use "closed_loop" or "open_loop"'.format(
                req.state.data))

        return set_modeResponse()


    def setGainsCB(self, req):
        self.GAIN_TRANSLATION = req.gain_position
        self.GAIN_ROTATION    = req.gain_rotation
        rospy.logerr('Set Gain position: {:.1f} and rotation: {:.1f}'.format(self.GAIN_TRANSLATION, self.GAIN_ROTATION))

        res = set_gainsResponse()
        res.completed = True

        return res



    def planPathCB(self, goal):
        resultMsg = mm.move2poseResult()

        if self.control_mode != 'open_loop':
            resultMsg.completed = False
            resultMsg.additionalInfo = 'Mechanical arena is not in open_loop control!'
            self.move2poseActionServer.set_preempted(resultMsg, text=resultMsg.additionalInfo)
            rospy.logerr('Mechanical arena is not in open_loop control!')
            return

        rospy.logerr('Mechanical arena received goal!')

        x_goal = goal.goal_pose.position.x
        y_goal = goal.goal_pose.position.y
        goal_pos = np.array([x_goal, y_goal])
        start_pos = -np.array(self.xy_motor_pos)

        if self.pathplanner is not None:

            rospy.logerr('START POS: {:.2f} {:.2f}'.format(start_pos[0],start_pos[1]))
            path = self.pathplanner.getPath(start_pos, goal_pos)

            if len(path) == 2:
                path =[goal_pos]
            if len(path) > 2:
                path = list(path[1:])
                path.append(goal_pos)


            cancelledGoal = False
            for i in range(len(path)):
                if self.move2poseActionServer.is_preempt_requested():
                    cancelledGoal = True
                    break


                self.move2posCB(path[i], velocity = goal.velocity.linear.x)
                time.sleep(0.01)


            if cancelledGoal:
                posCmd = [0] * 2
                velCmd = [0] * 2
                self.xyDriver.setCMD(posCmd, velCmd)
                resultMsg.completed = False
                resultMsg.additionalInfo = 'goal cancelled by client'
                self.move2poseActionServer.set_preempted(resultMsg, text=resultMsg.additionalInfo)
                return

        else:
            self.move2posCB(goal_pos, velocity=goal.velocity.linear.x)
            time.sleep(0.01)




        ########## TH PLANNER  #############
        time.sleep(0.1)

        q = [goal.goal_pose.orientation.x, goal.goal_pose.orientation.y, goal.goal_pose.orientation.z,
             goal.goal_pose.orientation.w]
        (_, _, th_goal) = tf.transformations.euler_from_quaternion([q[0], q[1], q[2], q[3]])
        th_goal = np.degrees(th_goal)
        th_goal = th_goal % 360
        # th_increment = limitValue( th_goal , 360)   # in degrees
        #motor_th = (th_goal / 360.0) * (float(
        #    ARENA_GEAR_THEETH_NUMBER) / TH_MOTOR_GEAR_THEETH_NUMBER)  # turns = arena_ang(Deg)/360 * motor_ang(Deg)/arena_ang(Deg)

        self.getArenaPosition()
        dist = th_goal - self.th_pos
        rospy.logerr('INIT: TH_GOAL: {:.2f}, TH_POS {:.2f}, dist {:.2f}'.format(th_goal, self.th_pos, dist))

        if dist > 0:
            if dist < 180:
                self.thDriver.set_velocity(-VEL_POS_CONTROL_TH)
            else:
                self.thDriver.set_velocity(VEL_POS_CONTROL_TH)

        elif dist < 0:
            if dist < -180:
                self.thDriver.set_velocity(-VEL_POS_CONTROL_TH)
            else:
                self.thDriver.set_velocity(VEL_POS_CONTROL_TH)

        timer = time.time()

        while np.abs(dist) > 1:
            if self.move2poseActionServer.is_preempt_requested():
                break

            if time.time() - timer > 30:
                break

            self.getArenaPosition()
            dist = th_goal - self.th_pos
            #rospy.logerr('INIT: TH_GOAL: {:.2f}, TH_POS {:.2f}, dist {:.2f}'.format(th_goal, self.th_pos, dist))

            # rospy.logerr('WHILE: TH_GOAL: {:.2f}, TH_POS {:.2f}, dist {:.2f}'.format(th_goal, self.th_pos, dist))

        # self.thDriver.set_pos(motor_th - self.thDriver.get_pos())
        self.thDriver.set_velocity(0)

        # while dist > 3 and dist < 360-3:
        #    dist = np.abs(limitValue( th_goal - self.th_pos, 360))




        rospy.logerr('GOAL COMPLETED!!!!')

        resultMsg.completed = True
        resultMsg.additionalInfo = 'Mechanical arena reached position'
        self.move2poseActionServer.set_succeeded(resultMsg)


    def move2posCB(self, goal, velocity):


        x_goal = goal[0]
        y_goal = goal[1]
        goal_pos = np.array([x_goal, y_goal])

        '''


        ----------------------

        pos = np.array(point.positions) + self.offsetPosition
        currentPos = np.array(self.manipulator.positionState)
        posCmd = pos - currentPos

        max_vel = np.max(point.velocities)
        max_pos = np.max(np.abs(posCmd))
        if max_pos != 0:
            vel = np.true_divide(np.abs(posCmd), max_pos) * max_vel
            vel = np.clip(vel, 50, None).astype(np.int)

        else:
            vel = np.array(point.velocities)

        # acc = np.array(point.accelerations)


        vel[posCmd == 0] = 0
        self.manipulator.sendCmd(posCmd, vel)

        dist = 1

        -----------------
        '''

        v = velocity * XY_MOTOR_STEP_PER_MM  # steps/s  = mm/s * steps/mm

        motor_x = - x_goal * XY_MOTOR_STEP_PER_MM + self.xy_motor_offset[0]  # steps  = mm * steps
        motor_y = - y_goal * XY_MOTOR_STEP_PER_MM + self.xy_motor_offset[1]  # steps  = mm * steps

        if motor_x == 0:
            motor_x = 1
        if motor_y == 0:
            motor_y = 1

        pos = [int(motor_x), int(motor_y)]

        posCmd = np.array(pos) - np.array(self.xyDriver.position)
        max_pos = np.max(np.abs(posCmd))
        if max_pos != 0:
            vel = np.true_divide(np.abs(posCmd), max_pos) * v
            vel = np.clip(vel, 50, None).astype(np.int)
            vel = [int(vel[0]), int(vel[1])]
        else:
            vel = [int(v), int(v)]

        vel = [int(v), int(v)]  # comment for straight line trajectory

        self.xyDriver.setCMD(pos, vel)

        dist = 10000
        cancelledGoal = False

        timer = time.time()
        while dist > 1:
            curr_pos = - np.array(self.xy_motor_pos)
            dist = np.linalg.norm(curr_pos - goal_pos)
            if time.time() - timer > 10: #seconds
                rospy.logerr('Stuck in while for position!')
                break




    def getDataDirCB(self, req):

        DIR = req.dir
        FILENAME = req.base_name

        if not os.path.exists(DIR):
            os.makedirs(DIR)
        self.loggingData = False
        time.sleep(0.1)
        ############### LOGGER #####################
        if self.df is not None:
            self.df.closeFile()

        self.df = dataFile(DIR, FILENAME, maxRowsPerFile=10 ** 5)
        self.df.setColumnFields(
            ["time_stamp", "control_mode", "pos_x", "pos_y", "theta", "vel_x", "vel_y", "vel_z", "ball_x", "ball_y",
             "ball_z"])
        self.timerLogData = time.time()
        self.loggingData = True

        res = set_log_directoryResponse()
        res.completed = True
        return res

    def trackingDataCB(self, data):
        self.ball_tracking_data = data

    def manualVelCmdCB(self, velCmd):
        self.manualVel = velCmd

    def DisplacementCmdCB(self, dispCmd):
        self.ballDispCmd = dispCmd




    """ Code for the main thread of the node """

    def mainThread(self):
        dt_disp = time.time() - self.timer
        self.timer = time.time()




        self.xyDriver.mainLoop()
        self.getArenaPosition()
        self.publishFlyPose()

        self.root.update()  # for gui
        if self.gui_control.requestedHome:
            self.setHome()
            self.gui_control.requestedHome = False

        if self.gui_control.state == 'stop':
            self.velCmd = TwistStamped()  # For zero velocities
            self.xyDriver.setCMD([0, 0], [0,0])



        if self.gui_control.state == 'manual':
            self.dispCmd.angular.z += self.manualVel.twist.angular.z * dt_disp
            self.dispCmd.linear.x  += 5*self.manualVel.twist.linear.x *   dt_disp
            self.dispCmd.linear.y  += 5*self.manualVel.twist.linear.y *   dt_disp


        if self.gui_control.state == 'run':
            self.dispCmd.angular.z = self.ballDispCmd.angular.z
            self.dispCmd.linear.x  = self.ballDispCmd.linear.x
            self.dispCmd.linear.y  = 0#self.ballDispCmd.linear.y




        if self.gui_control.state != self.gui_control.prevState or self.control_mode == 'open_loop':
            self.prevDispCmd.linear.x = self.dispCmd.linear.x
            self.prevDispCmd.linear.y = self.dispCmd.linear.y

            self.posth_offset_ball = self.GAIN_ROTATION*self.dispCmd.angular.z - np.radians(self.thDriver.get_abs_pos()*360)/ (float(ARENA_GEAR_THEETH_NUMBER) / TH_MOTOR_GEAR_THEETH_NUMBER )
            self.posx_offset_ball = self.dispCmd.linear.x
            self.posy_offset_ball = self.dispCmd.linear.y

        self.gui_control.prevState = self.gui_control.state



        if self.control_mode == 'closed_loop':
            if self.gui_control.state == 'run':
                self.logData()

            self.posth_motor_command =  np.degrees(self.GAIN_ROTATION*(self.dispCmd.angular.z )- self.posth_offset_ball) * float(ARENA_GEAR_THEETH_NUMBER) / TH_MOTOR_GEAR_THEETH_NUMBER / 360.0  # turns/s = arena_ang(Deg)/s * motor_ang(Deg)/arena_ang(Deg) /360

            self.thDriver.set_pos(self.posth_motor_command)



            ball_velx = -self.GAIN_TRANSLATION*(self.dispCmd.linear.x - self.prevDispCmd.linear.x)/dt_disp
            ball_vely = self.GAIN_TRANSLATION*(self.dispCmd.linear.y - self.prevDispCmd.linear.y)/dt_disp

            self.prevDispCmd.linear.x = self.dispCmd.linear.x
            self.prevDispCmd.linear.y = self.dispCmd.linear.y


            velx = ball_velx * (180 / np.pi) * XY_MOTOR_STEP_PER_MM  # steps/s  = mm/s * steps/mm
            vely = ball_vely * (180 / np.pi) * XY_MOTOR_STEP_PER_MM  # steps/s  = mm/s * steps/mm

            #### COLISIION ####
            velxMotor = velx * np.sin(self.th_pos * np.pi / 180) - vely * np.cos(self.th_pos * np.pi / 180)
            velyMotor = -velx * np.cos(self.th_pos * np.pi / 180) - vely * np.sin(self.th_pos * np.pi / 180)
            # velxyMotors = np.round( clipVelocity(self.xy_motor_pos, [velxMotor, velyMotor], self.wallCollision ), 0 )
            velxyMotors = np.round(self.collision.clipVelocity(self.xy_motor_pos, [velxMotor, velyMotor]), 0)

            #velxyMotors = [velxMotor, velyMotor]
            
            
            try:
                self.set_xyvel = [int(velxyMotors[0]), int(velxyMotors[1])]
                self.xyDriver.setCMD([0, 0], self.set_xyvel)

            except:
                rospy.logerr("Velocities are NaN")
                self.set_xyvel = [0.0, 0.0]






    def setHome(self):
        self.th_pos = INITIAL_ANGLE_TH
        self.xy_motor_pos = INITIAL_POSITION_XY

        self.th_motor = 360 * float(self.thDriver.get_pos())

        self.xyDriver.getMotorState()
        self.xy_motor_offset = np.array(self.xyDriver.position) + np.array(INITIAL_POSITION_XY) * XY_MOTOR_STEP_PER_MM
        print(self.xy_motor_offset)


    def getArenaPosition(self):
        current_th_motor =  360*float(self.thDriver.get_pos())
        dthang = math.fmod((current_th_motor - self.th_motor) + 180 * 3, 2 * 180) - 180
        self.th_motor = current_th_motor

        self.th_pos += dthang * float(TH_MOTOR_GEAR_THEETH_NUMBER) / ARENA_GEAR_THEETH_NUMBER
        self.th_pos = limitValue(self.th_pos, 360)


        self.xy_motor_pos = np.array(self.xyDriver.position) - self.xy_motor_offset
        self.xy_motor_pos = np.true_divide(self.xy_motor_pos, XY_MOTOR_STEP_PER_MM)


    def logData(self):
        if self.df is None:
            return

        vel_xy_motor = np.true_divide(self.set_xyvel, XY_MOTOR_STEP_PER_MM)
        vel_th = 360 * float(self.set_thvel / ENCODER_TH_MOTOR_STEPS_PER_REV) * float(
            TH_MOTOR_GEAR_THEETH_NUMBER) / ARENA_GEAR_THEETH_NUMBER

        if time.time() - self.timerLogData > (1. / SAVE_DATA_FREQ) and self.loggingData:
            # add data
            ball_tracking = [self.ball_tracking_data.point.x, self.ball_tracking_data.point.y,
                             self.ball_tracking_data.point.z]
            self.df.addRow([rospy.Time.now(), self.control_mode,
                            self.xy_motor_pos[0], self.xy_motor_pos[1], self.th_pos,
                            vel_xy_motor[0], vel_xy_motor[1], vel_th,
                            ball_tracking[0], ball_tracking[1], ball_tracking[2]])

            self.timerLogData = time.time()

    def publishFlyPose(self):
        posArena = np.true_divide(self.xy_motor_pos, 1000)  # In meters

        if time.time() - self.flyPosPubTimer > 1.0 / PUBLISH_FLY_POSE_FREQ:
            fly_pose = PoseStamped()
            fly_pose.header.stamp = rospy.Time.now()
            fly_pose.header.frame_id = 'base_link'
            fly_pose.pose.position.x = -posArena[0]
            fly_pose.pose.position.y = -posArena[1]
            quaternion = transformations.quaternion_from_euler(0, 0, self.th_pos* (np.pi / 180))
            fly_pose.pose.orientation.x = quaternion[0]
            fly_pose.pose.orientation.y = quaternion[1]
            fly_pose.pose.orientation.z = quaternion[2]
            fly_pose.pose.orientation.w = quaternion[3]
            self.flyPosPub.publish(fly_pose)

            self.tfBr.sendTransform((-posArena[0], -posArena[1], 0),
                                    tf.transformations.quaternion_from_euler(0, 0, self.th_pos * (np.pi / 180)),
                                    rospy.Time.now(),
                                    "fly",
                                    "base_link")



            if self.collision.wall is not None:
                if time.time() - self.collisionPubTimer > 1./PUBLISH_POLYGON:
                    collMsg = PolygonStamped()
                    collMsg.polygon.points = []
                    for i in range(len(self.collision.wall_mm[:,0])):
                        p = Point32()

                        p.x = self.collision.wall_mm[i,0]
                        p.y = self.collision.wall_mm[i,1]
                        collMsg.polygon.points.append(p)

                    collMsg.header.frame_id = 'base_link'
                    collMsg.header.stamp = rospy.Time.now()

                    self.collisionPub.publish(collMsg)

                    self.collisionPubTimer = time.time()


            self.flyPosPubTimer = time.time()




    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('mechanical_vr_driver_node', anonymous=True)
        rate = rospy.Rate(1000)  # 10 Hz
        node = MechanicalVrDriver(None)

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()
        node.df.closeFile()
    except rospy.ROSInterruptException:
        pass

