
from thMotor_interface import thMotorInterfaceAPI
from settings import *


POS_GAIN_TH_MOTOR_POSITION_CONTROL            = 20
VEL_GAIN_TH_MOTOR_POSITION_CONTROL            =  0.2   #  turns / s
VEL_INTEGRATOR_GAIN_TH_MOTOR_POSITION_CONTROL =  0.1   # Nm/((turn/s) * s

thDriver = thMotorInterfaceAPI()
thDriver.connect()
#thDriver.engage()  # get ready to drive
thDriver.setControlGains(POS_GAIN_TH_MOTOR_POSITION_CONTROL, VEL_GAIN_TH_MOTOR_POSITION_CONTROL, VEL_INTEGRATOR_GAIN_TH_MOTOR_POSITION_CONTROL)

thDriver.setPositionControl()


while True:
    thDriver.set_pos(1)

