import csv
import os


def ensure_dir(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)


class dataFile:
    def __init__(self, path, name, maxRowsPerFile = 30*60):
        self.path = path
        self.name = name

        dir = os.path.dirname(self.path)
        if not os.path.exists(dir):
            os.makedirs(dir)
        self.maxRowsPerFile = maxRowsPerFile
        self.fields = []
        self.numRows = 0
        self.fileIndex = 0
        self.filename = None
        self.createFile()


    def createFile(self):
        self.fileIndex += 1
        self.numRows = 0
        self.filename = self.path + self.name + '_' + str(self.fileIndex) + ".csv"
        self.cvsfile = None
        self.openForAppend()

    def setColumnFields(self, fields):
        self.fields = fields

    def openForAppend(self):
        if self.cvsfile:
            self.cvsfile.close()
        self.cvsfile = open(self.filename, 'a')
        self.writer = csv.writer(self.cvsfile)
        self.addRow(self.fields)

    def openForRead(self):
        if self.cvsfile:
            self.cvsfile.close()

        self.cvsfile= open(self.filename, 'r')
        self.reader = csv.reader(self.cvsfile, delimiter=',', quotechar='|')

    def readData(self):
        data = []
        fields = []
        for row in self.reader:
            for i in range(len(row)):
                data.append([])
                fields.append(row[i])
            break

        for row in self.reader:
            for i in range(len(row)):
                try:
                    data[i].append(float(row[i]))
                except:
                    pass

        return data, fields





    def addRow(self, field):
        if self.numRows >= self.maxRowsPerFile:
            self.closeFile()
            self.createFile()
        self.writer.writerow(field)
        self.numRows += 1

    def closeFile(self):
        self.cvsfile.close()



######### Example of use ##############

"""
df = dataFile("C:\\Users\\flores\\Dropbox\\experimental_data\\sleep_deprivation\\test_1", "data")
df.openForAppend()

# add columns
df.addRow(["index","value1","value2"])

# add data
df.addRow([0, 5.5, 6.7])
df.addRow([1, 3.5, 6.3])
df.addRow([2, 7.5, 6.1])

df.closeFile()
"""
