#from Tkinter import *
import Tkinter


BUTTON_INTERSPACE_X = 100
BUTTON_INTERSPACE_Y = 0

class GUIcontrol(Tkinter.Frame):
    def __init__(self, master=None):
        Tkinter.Frame.__init__(self, master)
        self.master = master

        # widget can take all window
        self.pack(fill=Tkinter.BOTH, expand=1)

        # create button, link it to clickExitButton()
        self.state = 'manual'
        self.prevState = 'run'
        runButtonCB   = Tkinter.Button(self, text="RUN",      command=self.runButtonCB)
        manualButton  = Tkinter.Button(self, text="MANUAL",   command=self.manualButtonCB)
        stopButton    = Tkinter.Button(self, text="STOP",     command=self.stopButtonCB)
        setHomeButton = Tkinter.Button(self, text="SET HOME", command=self.setHomeButtonCB)

        self.stateLabel = Tkinter.Label(self.master, text=self.state,
                                 font=('Arial', 20), fg='red')

        self.stateLabel.place(x=BUTTON_INTERSPACE_X*2, y= 50)
        self.buttons = [runButtonCB, manualButton, stopButton, setHomeButton]
        for i in range(len(self.buttons)):
            button = self.buttons[i]
            button.place(x=i*BUTTON_INTERSPACE_X, y=i*BUTTON_INTERSPACE_Y)

        self.requestedHome = False

    def runButtonCB(self):
        self.prevState = self.state
        self.state = 'run'
        self.stateLabel['text'] = self.state

    def manualButtonCB(self):
        self.prevState = self.state
        self.state = 'manual'
        self.stateLabel['text'] = self.state

    def stopButtonCB(self):
        self.prevState = self.state
        self.state = 'stop'
        self.stateLabel['text'] = self.state

    def setHomeButtonCB(self):
        self.requestedHome = True

if __name__ == '__main__':
    root = Tkinter.Tk()
    app = GUIcontrol(root)
    root.wm_title("Mechanical Arena GUI Control")
    root.geometry('{:d}x{:d}'.format(BUTTON_INTERSPACE_X*4, 100))
    while True:
        root.update()
    #root.mainloop()