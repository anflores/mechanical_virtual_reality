import rospy

class logger:
    def __init__(self, ros = True):
        self.ros_logger = ros

    def info(self, msg):
        if self.ros_logger:
            rospy.loginfo(msg)
        else:
            print("INFO: " + msg)

    def warn(self, msg):
        if self.ros_logger:
            rospy.logwarn(msg)
        else:
            print("WARNING: " + msg)

    def error(self, msg):
        if self.ros_logger:
            rospy.logerr(msg)
        else:
            print("ERROR: " + msg)

    def debug(self, msg):
        if self.ros_logger:
            rospy.logdebug(msg)
        else:
            print("INFO: " + msg)
