import serial
import time
import numpy as np
from settings import *
from serialCom import SerialCom, serial_ports
import pygame
import rospy



def log_screen(typ, info, logger):
    if logger != "ROS":
        print(typ + ": " + info)

    else:
        if typ == "INFO":
            rospy.loginfo(info)
        elif typ == "WARNING":
            rospy.logwarn(info)
        if typ == "ERROR":
            rospy.logerr(info)


class xyMotors:
    def __init__(self, name, logger = "ROS"):
        self.ard = None
        self.name = name
        self.numMotors = 0
        self.nameMotors = []
        self.logger = logger

        self.position = [0,0]
        self.velocity = [0,0]

        self.setpositions  = [0,0]
        self.setvelocities = [0,0]

        self.sendCmdTimer = time.time()


    def getInfo(self):
        self.ard.connection.flushInput()
        self.ard.connection.flushOutput()

        self.ard.connection.write('info\n')
        time.sleep(0.5)
        r = self.ard.connection.readline().split(',')
        if r[0] == '':
            params = []
            return False, params

        name = r[0]
        numMotors = int(r[1])
        nameMotors = []
        for i in range(numMotors - 1):
            nameMotors.append(r[2 + i])

        nameMotors.append(r[2 + numMotors - 1].split('\r')[0])
        params = [name, numMotors, nameMotors]
        return True, params


    def connect(self):

        self.ard = SerialCom(XY_SERIAL_PORT, baud = ARD_BAUD_RATE)
        self.ard.open()
        time.sleep(2)

        r, params = self.getInfo()

        if self.name == params[0]:
            foundDriver = True


        if foundDriver:
            self.numMotors  = params[1]
            self.nameMotors = params[2]
            log_screen("INFO", " " * 5 + "Driver found!", self.logger)
            log_screen("INFO", " " * 5 + "Port driver: " + XY_SERIAL_PORT, self.logger)
            log_screen("INFO", " " * 5 + "Name driver: " + self.name, self.logger)
            log_screen("INFO", " " * 5 + "Number of motors:      " + str(self.numMotors), self.logger)
            log_screen("INFO", " " * 5 + "Name of the motors:    " + str(self.nameMotors), self.logger)
        else:
            log_screen("ERROR", " "*5 + "Driver not found!", self.logger)
            raise ('Motors xy driver not found!')

        return foundDriver


    def start(self):
        self.ard.connection.write('start ' + str(ARD_RECEIVE_FEEDBACK_FREQ) + '\n')   # Send arduino command_line to send motors state
        self.timer_reciv_data = time.time()
        time.sleep(0.1)



    def getMotorState(self):
        self.ard.reset_rxBuffer()  # This resets the index to read  data from the receiving buffer

        ###################################################################
        # Wait for a response and report any errors while receiving packets
        ###################################################################
        try:
            while not self.ard.available():
                if self.ard.status < 0:
                    if self.ard.status == -1:
                        log_screen("ERROR", 'ERROR: CRC_ERROR', self.logger)
                    elif self.ard.status == -2:
                        log_screen("ERROR", 'ERROR: PAYLOAD_ERROR', self.logger)
                    elif self.ard.status == -3:
                        log_screen('ERROR:', 'STOP_BYTE_ERROR', self.logger)

            ###################################################################
            # Parse response list
            ###################################################################
            positions = self.ard.rx_obj(obj=self.position)
            velocities = self.ard.rx_obj(obj=self.velocity)

            if INVERT_X_MOTOR:
                positions[0] = -positions[0]
                velocities[0] = -velocities[0]

            if INVERT_Y_MOTOR:
                positions[1] = -positions[1]
                velocities[1] = -velocities[1]

            self.position = positions
            self.velocity = velocities
        except Exception as e:
            rospy.logerr(e)

    def mainLoop(self):
        self.getMotorState()
        if time.time() - self.sendCmdTimer > 1.0/ARD_SEND_COMMANDS_FREQ:
            self.sendCMD()
            self.sendCMDTimer = time.time()



    def setCMD(self, positions, velocities):
        if INVERT_X_MOTOR:
            positions[0] = -positions[0]
            velocities[0] = -velocities[0]


        if INVERT_Y_MOTOR:
            positions[1] = -positions[1]
            velocities[1] = -velocities[1]

        self.setpositions = positions
        self.setvelocities = velocities
        #self.ard.reset_txBuffer()  # This resets the index to append new data in the transmission buffer


    def sendCMD(self):
        self.ard.reset_txBuffer()  # This resets the index to append new data in the transmission buffer
        self.ard.tx_obj(self.setpositions)
        self.ard.tx_obj(self.setvelocities)
        self.ard.send()



if __name__ == "__main__":
    pygame.init()

    xyDriver = xyMotors("mechanical_arena_xy", logger=None)
    xyDriver.connect()
    xyDriver.start()


    pygame.font.init()  # you have to call this at the start,
    textFont = pygame.font.SysFont('Comic Sans MS', 30)
    screen = pygame.display.set_mode((640, 480))
    screen.fill((0, 0, 0))
    text = []
    text.append(textFont.render('Use the keys "w", "s", "a", "d" to move', True, (255, 255, 255)) )
    text.append(textFont.render('the translational motors', True, (255, 255, 255)) )
    for i in range(len(text)):
        screen.blit(text[i], (100, 200 + i*30))
    pygame.display.update()



    pressed_left  = False
    pressed_right = False
    pressed_up    = False
    pressed_down  = False



    while True:
        # event loop
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:  # check for key presses
                if event.key == pygame.K_a:  # left arrow turns left
                    pressed_left = True
                elif event.key == pygame.K_d:  # right arrow turns right
                    pressed_right = True
                elif event.key == pygame.K_w:  # up arrow goes up
                    pressed_up = True
                elif event.key == pygame.K_s:  # down arrow goes down
                    pressed_down = True




            elif event.type == pygame.KEYUP:  # check for key releases
                if event.key == pygame.K_a:  # left arrow turns left
                    pressed_left = False
                elif event.key == pygame.K_d:  # right arrow turns right
                    pressed_right = False
                elif event.key == pygame.K_w:  # up arrow goes up
                    pressed_up = False
                elif event.key == pygame.K_s:  # down arrow goes down
                    pressed_down = False

        pos = [0, 0]
        vel = [0, 0]
        velxy = 3000

        if pressed_left:
            vel[0] = -velxy
        if pressed_right:
            vel[0] =  velxy
        if pressed_up:
            vel[1] = velxy
        if pressed_down:
            vel[1] = -velxy


        xyDriver.mainLoop()

        print(vel, xyDriver.position, xyDriver.velocity)
        xyDriver.setCMD(pos, vel)

        time.sleep(0.001)

