import serial
from serial.serialutil import SerialException

import sys
import time
import logging
import traceback

import odrive
from odrive.enums import *
import math
import pygame
from logger import logger
import rospy
from settings import *
import fibre
import rospy
from fibre import ChannelBrokenException, ChannelDamagedException

default_logger = logging.getLogger(__name__)
default_logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

default_logger.addHandler(ch)


class ODriveFailure(Exception):
    pass


class thMotorInterfaceAPI(object):
    driver = None
    encoder_cpr = 4096
    right_axis = None
    left_axis = None
    connected = False
    _preroll_started = False
    _preroll_completed = False

    # engaged = False

    def __init__(self, logger_type="default", active_odrive=None):
        self.th_motor_pos = 0


        if logger_type == "ROS":
            self.logger = logger(ros=True)
        else:
            self.logger = logger(ros=False)

        if active_odrive:  # pass in the odrv0 object from odrivetool shell to use it directly.
            self.driver = active_odrive
            self.axes = self.driver.axis0
            self.logger.info("Loaded pre-existing ODrive interface. Check index search status.")
            self.encoder_cpr = self.driver.axis0.encoder.config.cpr
            self.connected = True
            self._preroll_started = False
            self._preroll_completed = True

    def __del__(self):
        self.disconnect()

    def update_time(self, curr_time):
        # provided so simulator can update position
        pass

    def connect(self, port=None, timeout=30):
        if self.driver:
            self.logger.info("Already connected. Disconnecting and reconnecting.")
        try:
            if port is not None:
                print(port)
                self.driver = odrive.find_any(port, timeout=timeout, logger=self.logger)
            else:
                self.driver = odrive.find_any(timeout=timeout, logger=self.logger)

            rospy.loginfo("CONNECTED TO ODRIVE")
            self.axes = self.driver.axis0
            self.driver.axis0 = self.driver.axis0

        except:
            rospy.logerr("No ODrive found. Is device powered?")

            return False

        # save some parameters for easy access
        self.driver.axis0 = self.driver.axis0

        # check for no errors
        if self.driver.axis0.error != 0:
            error_str = "Had error on startup, rebooting. Axis error 0x%x, motor error 0x%x, encoder error 0x%x. Rebooting." % (
            self.driver.axis0.error, self.driver.axis0.motor.error, self.driver.axis0.encoder.error)
            self.logger.error(error_str)
            self.reboot()
            return False

        self.encoder_cpr = self.driver.axis0.encoder.config.cpr

        self.connected = True
        self.logger.info("Connected to ODrive. " + self.get_version_string())

        self._preroll_started = False
        self._preroll_completed = False
        return True

    def disconnect(self):
        self.connected = False
        self.driver.axis0 = None

        # self.engaged = False

        if not self.driver:
            self.logger.error("Not connected.")
            return False

        try:
            self.release()
        except:
            self.logger.error("Error in timer: " + traceback.format_exc())
            return False
        finally:
            self.driver = None
        return True

    def get_version_string(self):
        if not self.driver or not self.connected:
            return "Not connected."
        return "ODrive %s, hw v%d.%d-%d, fw v%d.%d.%d%s, sdk v%s" % (
            str(self.driver.serial_number),
            self.driver.hw_version_major, self.driver.hw_version_minor, self.driver.hw_version_variant,
            self.driver.fw_version_major, self.driver.fw_version_minor, self.driver.fw_version_revision,
            "-dev" if self.driver.fw_version_unreleased else "",
            odrive.version.get_version_str())

    def reboot(self):
        if not self.driver:
            self.logger.error("Not connected.")
            return False
        try:
            self.driver.reboot()
        except KeyError:
            self.logger.error("Rebooted ODrive.")
        except:
            self.logger.error("Failed to reboot: " + traceback.format_exc())
        finally:
            self.driver = None
        return True

    def calibrate(self):
        if not self.driver:
            self.logger.error("Not connected.")
            return False
        print("calibration 1")
        self.logger.info("Vbus %.2fV" % self.driver.vbus_voltage)

        self.logger.info("Calibrating axis %d..." % 0)
        self.driver.axis0.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE
        time.sleep(1)
        while self.driver.axis0.current_state != AXIS_STATE_IDLE:
            time.sleep(0.1)
        if self.driver.axis0.error != 0:
            self.logger.error(
                "Failed calibration with axis error 0x%x, motor error 0x%x" % (self.driver.axis0.error, self.driver.axis0.motor.error))
            return False

        self.driver.axis0.motor.config.pre_calibrated               = True
        self.driver.axis0.motor.config.current_lim                  = 60

        self.driver.axis0.config.startup_encoder_offset_calibration = True
        self.driver.axis0.config.startup_closed_loop_control        = True
        self.driver.save_configuration()
        #self.driver.axis0.reboot()

        return True




    def preroll(self, wait=True):
        if not self.driver:
            self.logger.error("Not connected.")
            return False

        if self._preroll_started:  # must be prerolling or already prerolled
            return False
        self._preroll_started = True
        self._preroll_completed = False

        # self.logger.info("Vbus %.2fV" % self.driver.vbus_voltage)

        self.logger.info("Index search preroll axis %d..." % 0)
        self.driver.axis0.requested_state = AXIS_STATE_ENCODER_INDEX_SEARCH

        if wait:
            while self.driver.axis0.current_state != AXIS_STATE_IDLE:
                time.sleep(0.1)
            self._preroll_started = False
            if self.driver.axis0.error != 0:
                self.logger.error(
                    "Failed preroll with left_axis error 0x%x, motor error 0x%x" % (self.driver.axis0.error, self.driver.axis0.motor.error))
                return False
            self._preroll_completed = True
            self.logger.info("Index search preroll complete.")
            return True
        else:
            return False

    def ensure_prerolled(self):
        # preroll success
        if self._preroll_completed:
            return True
        # started, not completed
        elif self._preroll_started:
            # self.logger.info("Checking for preroll complete.")
            if self.driver.axis0.current_state != AXIS_STATE_ENCODER_INDEX_SEARCH:
                # completed, check for errors before marking complete
                if self.driver.axis0.error != 0:
                    self._preroll_started = False
                    error_str = "Failed index search preroll with axis error 0x%x, motor error 0x%x, encoder error 0x%x. Rebooting." % (
                        self.driver.axis0.error, self.driver.axis0.motor.error, self.driver.axis0.encoder.error)
                    # self.reboot()
                    self.logger.error(error_str)
                    raise Exception(error_str)
                # no errors, success
                self._preroll_started = False
                self._preroll_completed = True
                self.logger.info("Index search preroll complete. Ready to drive.")
                return True
            else:
                # still prerolling
                return False
        else:  # start preroll
            # self.logger.info("Preroll started.")
            self.preroll(wait=False)
            return False

    def has_prerolled(self):
        return self._preroll_completed

    def engaged(self):
        if self.driver and hasattr(self, 'axes'):
            return self.driver.axis0.current_state == AXIS_STATE_CLOSED_LOOP_CONTROL
        else:
            return False

    def idle(self):
        if self.driver and hasattr(self, 'axes'):
            return self.driver.axis0.current_state == AXIS_STATE_IDLE
        else:
            return False

    def engage(self):
        if not self.driver:
            self.logger.error("Not connected.")
            return False

        # self.logger.debug("Setting drive mode.")
        self.driver.axis0.controller.input_vel = 0
        self.driver.axis0.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
        self.setVelocityControl()
        # self.engaged = True
        return True

    def setVelocityControl(self):
        self.driver.axis0.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
        self.driver.axis0.controller.config.control_mode = CONTROL_MODE_VELOCITY_CONTROL


    def setPositionControl(self):#, vel_limit, acc_limit, dec_limit, intertia):
        self.set_pos(self.get_pos())
        self.driver.axis0.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
        self.driver.axis0.controller.config.control_mode = CONTROL_MODE_POSITION_CONTROL
        #self.driver.axis0.controller.config.input_mode = INPUT_MODE_TRAP_TRAJ

        #self.driver.axis0.controller.config.vel_limit =  vel_limit
        #self.driver.axis0.controller.config.accel_limit = acc_limit
        #self.driver.axis0.controller.config.decel_limit = dec_limit
        #self.driver.axis0.controller.config.inertia = intertia


    def setCurrentLimit(self, current):
        self.driver.axis0.motor.config.current_lim = current

    def setControlGains(self, pos_gain = 20.0, vel_gain = 0.16, vel_integrator_gain = 0.32):
        self.driver.axis0.controller.config.pos_gain            =  pos_gain               # counts / s
        self.driver.axis0.controller.config.vel_gain            =  vel_gain               # counts / s
        self.driver.axis0.controller.config.vel_integrator_gain =  vel_integrator_gain    # (counts / s) * s


    def release(self):
        if not self.driver:
            self.logger.error("Not connected.")
            return False
        # self.logger.debug("Releasing.")

        self.driver.axis0.requested_state = AXIS_STATE_IDLE

        # self.engaged = False
        return True

    def set_velocity(self, motor_val):
        if not self.driver:
            self.logger.error("Not connected.")
            return
        try:

            self.driver.axis0.controller.input_vel = -motor_val
        except (fibre.protocol.ChannelBrokenException, AttributeError) as e:
            rospy.logerr(e)

    def set_pos(self, absolute_pos):
        if not self.driver:
            self.logger.error("Not connected.")
            return

        try:
            self.driver.axis0.controller.input_pos = absolute_pos
        except (fibre.protocol.ChannelBrokenException, AttributeError) as e:
            rospy.logerr(e)

    def set_pos_incremental(self, incremental_pos):
        self.driver.axis0.controller.move_incremental( incremental_pos,  False )

    def set_incremental_pos(self, increment):
        # Use the move_incremental function to move to a relative position.
        # To set the goal relative to the current actual position, use from_goal_point = False
        # To set the goal relative to the previous destination, use from_goal_point = True
        self.driver.axis0.controller.move_incremental(increment, False)

    def feed_watchdog(self):
        self.driver.axis0.watchdog_feed()

    def get_errors(self, clear=True):
        # TODO: add error parsing, see: https://github.com/madcowswe/ODrive/blob/master/tools/odrive/utils.py#L34
        if not self.driver:
            return None

        axis_error = self.driver.axis0.error

        if axis_error:
            error_string = "Errors(hex): R: a%x m%x e%x c%x" % (
                self.driver.axis0.error, self.driver.axis0.motor.error, self.driver.axis0.encoder.error,
                self.driver.axis0.controller.error,
            )

        if clear:
            self.driver.axis0.error = 0
            self.driver.axis0.motor.error = 0
            self.driver.axis0.encoder.error = 0
            self.driver.axis0.controller.error = 0

        if axis_error:
            return error_string


    def vel_estimate(self):
        try:
            pos = self.driver.axis0.encoder.vel_estimate if self.driver.axis0 else 0  # neg is forward for right
            self.th_motor_pos = pos
        except Exception as e:
            rospy.logerr(e)

        return self.th_motor_pos

    def get_pos(self):
        return self.driver.axis0.encoder.pos_cpr if self.driver.axis0 else 0  # sign!

    def get_abs_pos(self):
        return self.driver.axis0.encoder.pos_estimate if self.driver.axis0 else 0  # sign!

    # TODO check these match the right motors, but it doesn't matter for now
    def get_temperature(self):
        return self.driver.axis0.motor.get_inverter_temp() if self.driver.axis0 else 0.

    def get_current(self):
        return self.driver.axis0.motor.current_control.Ibus if self.driver.axis0 and self.driver.axis0.current_state > 1 else 0.

    # from axis.hpp: https://github.com/madcowswe/ODrive/blob/767a2762f9b294b687d761029ef39e742bdf4539/Firmware/MotorControl/axis.hpp#L26
    MOTOR_STATES = [
        "UNDEFINED",  # <! will fall through to idle
        "IDLE",  # <! disable PWM and do nothing
        "STARTUP_SEQUENCE",  # <! the actual sequence is defined by the config.startup_... flags
        "FULL_CALIBRATION_SEQUENCE",  # <! run all calibration procedures, then idle
        "MOTOR_CALIBRATION",  # //<! run motor calibration
        "SENSORLESS_CONTROL",  # //<! run sensorless control
        "ENCODER_INDEX_SEARCH",  # //<! run encoder index search
        "ENCODER_OFFSET_CALIBRATION",  # //<! run encoder offset calibration
        "CLOSED_LOOP_CONTROL",  # //<! run closed loop control
        "LOCKIN_SPIN",  # //<! run lockin spin
        "ENCODER_DIR_FIND",
    ]


    def get_state(self):
        return self.MOTOR_STATES[self.driver.axis0.current_state] if self.driver.axis0 else "NOT_CONNECTED"

    def bus_voltage(self):
        return self.driver.vbus_voltage if self.driver.axis0 else 0.






if __name__ == '__main__':
    thDriver = thMotorInterfaceAPI()
    thDriver.connect()
    # self.thDriver.engage()  # get ready to drive
    thDriver.setVelocityGains(VEL_GAIN_TH_MOTOR, VEL_INTEGRATOR_GAIN_TH_MOTOR)