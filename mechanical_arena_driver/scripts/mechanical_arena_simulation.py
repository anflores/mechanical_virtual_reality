#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import TwistStamped, Pose, Polygon
from mechanical_vr_driver.srv import get_collision, get_collisionRequest
from collision import *

from log import dataFile
import tf
import numpy as np
import math
from settings import *
import time
import matplotlib.pyplot as plt

def plotWall(axis, wall, linewidth = 1, color = 'black'):
    wall_tmp = np.zeros([wall.shape[0] + 1, wall.shape[1]])
    wall_tmp[:wall.shape[0], :] = wall
    wall_tmp[-1, :] = wall[0]
    axis.plot(wall_tmp[:,0], wall_tmp[:,1], linewidth = linewidth, color = color)
    axis.plot()


def clipVelocity(posMotors, velMotors, wall):
    dist, proj_pos, tang_vect = getDistanceFromConvexHull(wall, posMotors)
    print(dist, posMotors)
    if dist > 0: # outside
        norm_vect = np.array([-tang_vect[1], tang_vect[0]])
        if np.dot(norm_vect, velMotors) > 0:
            vel_tang_arena = np.dot(velMotors, tang_vect) * tang_vect  # Project to tangent vector
            return vel_tang_arena
    return velMotors



def clipVelocity2(posMotors, velMotors):
    dist_from_origin = np.linalg.norm(posMotors)
    max_dist_from_origin = float(DIAMETER_ARENA)/2 - WIDTH_ARENA - DISTANCE_COLISION
    if dist_from_origin > max_dist_from_origin:
        ang_pos_arena = np.arctan2(posMotors[1], posMotors[0])
        norm_vector_arena = np.array([np.cos(ang_pos_arena), np.sin(ang_pos_arena)])
        if np.dot(norm_vector_arena, np.array(velMotors)) > 0:
            tang_vector_arena = np.array([np.sin(ang_pos_arena), - np.cos(ang_pos_arena)])
            vel_tang_arena = np.dot(np.array(velMotors), tang_vector_arena) * tang_vector_arena
            return vel_tang_arena

    return velMotors



def getWall(polyWall):
    x = []
    y = []
    for p in polyWall.points:
        x.append(p.x)
        y.append(p.y)
    wall = np.array([x,y]).T
    return wall*1000 # in mm




class MechanicalVrDriver:
    def __init__(self):
        self.started = False
        self.debug("INFO", "mechanical_vr_driver_node is running!")



        self.xy_motor_pos = INITIAL_POSITION_XY
        self.th_pos = INITIAL_ANGLE_TH

        ####### publisher ##########
        #self.pub = rospy.Publisher('chatter', String, queue_size=10)
        self.pub = ""

        ####### subscribers ########
        rospy.Subscriber("/ball_tracking/cmd_vel", TwistStamped, self.velCmdCB)
        self.subs = ""

        time.sleep(0.1)

        self.set_xyvel      = [0,0]
        self.set_pv_velxy   = [0,0]
        self.set_thvel      = 0
        self.set_prev_thvel = 0

        self.timer = time.time()

        self.filter_xy = 0.02
        self.filter_th = 0.02

        self.timer = time.time()

        self.xMotorVel_acc = []
        self.yMotorVel_acc = []
        self.thMotorVel_acc = []


        self.ave_time = 0.1 # sec

        self.df = dataFile( "/home/nci_la/mechanical_arena_test_data/", "data", maxRowsPerFile = 10**5)
        self.df.setColumnFields(["time_stamp", "pos_x", "pos_y", "theta"])
        self.timerLogData = time.time()


        rospy.loginfo("ARENA IS READY!")
        self.started = True

        self.tfBr = tf.TransformBroadcaster()


        ######### services #########
        self.getCollisionService = rospy.ServiceProxy('mechanical_arena/collision', get_collision)
        self.getCollisionService.wait_for_service()
        msg = self.getCollisionService()
        self.wallCollision = getWall(msg.collision)

        #fig, axis = plt.subplots(figsize = (10,10))
        #plotWall(axis, self.wallCollision)
        #plt.show()





    """ call backs functions """
    def velCmdCB(self, velCmd):
        if self.started:
            self.xMotorVel_acc.append(-velCmd.twist.linear.x * GAIN_TRANSLATION)
            self.yMotorVel_acc.append(-velCmd.twist.linear.y * GAIN_TRANSLATION)
            self.thMotorVel_acc.append(velCmd.twist.angular.z * GAIN_ROTATION)


            if time.time() - self.timer > TIME_PROCESSING_TRACKING_DATA:
                velx_ave  = np.mean(self.xMotorVel_acc)
                vely_ave  = np.mean(self.yMotorVel_acc)
                velth_ave = np.mean(self.thMotorVel_acc)



                #velx = velCmd.twist.linear.x * (self.filter_xy) + self.set_pv_velxy[0]*(1 - self.filter_xy)
                #vely = velCmd.twist.linear.y * (self.filter_xy) + self.set_pv_velxy[1]*(1 - self.filter_xy)
                #self.set_pv_velxy = [velx, vely]


                velx = velx_ave * (180/np.pi) * XY_MOTOR_STEP_PER_MM   # steps/s  = mm/s * steps/mm
                vely = vely_ave * (180/np.pi) * XY_MOTOR_STEP_PER_MM   # steps/s  = mm/s * steps/mm

                #### COLISIION ####


                velxMotor =  velx * np.sin(self.th_pos*np.pi/180) - vely * np.cos(self.th_pos*np.pi/180)
                velyMotor = -velx * np.cos(self.th_pos*np.pi/180) - vely * np.sin(self.th_pos*np.pi/180)
                velxyMotors = np.round( clipVelocity(self.xy_motor_pos, [velxMotor, velyMotor], self.wallCollision ), 0 )
                #velxyMotors = np.round( clipVelocity2(self.xy_motor_pos, [velxMotor, velyMotor] ), 0 )

                #velthMotor = velCmd.twist.angular.z*(self.filter_th) + self.set_prev_thvel * (1 - self.filter_th)
                #self.set_prev_thvel = velthMotor

                velthMotor = velth_ave * (180/np.pi) * float(ARENA_GEAR_THEETH_NUMBER)/TH_MOTOR_GEAR_THEETH_NUMBER * float(ENCODER_TH_MOTOR_STEPS_PER_REV)/360.0  # encoder_counts/s = arena_ang(Deg)/s * motor_ang(Deg)/arena_ang(Deg) * encoder_counts/360

                try:
                    self.set_xyvel = [int(velxyMotors[0]), int(velxyMotors[1])]
                    self.set_thvel = velthMotor
                except:
                    rospy.logerr("Velocities are NaN")
                    self.set_xyvel = [0,0]
                    self.set_thvel = 0


                self.xMotorVel_acc = []
                self.yMotorVel_acc = []
                self.thMotorVel_acc = []
                self.timer = time.time()


    """ Code for the main thread of the node """
    def mainThread(self):
        #self.pub.publish("hello")
        self.pub = ""


        self.getArenaPosition()



    def getArenaPosition(self):


        self.th_pos += self.set_thvel*0.00005
        if self.th_pos < 0:
            self.th_pos += 360
        elif self.th_pos > 360:
            self.th_pos -= 360


        self.xy_motor_pos[0] = self.xy_motor_pos[0] + self.set_xyvel[0]*0.00005
        self.xy_motor_pos[1] = self.xy_motor_pos[1] + self.set_xyvel[1]*0.00005




        posArena = np.true_divide( self.xy_motor_pos, 1000 )  # In meters

        self.tfBr.sendTransform((-posArena[0], -posArena[1], 0),
                                tf.transformations.quaternion_from_euler(0, 0, self.th_pos * (np.pi / 180)),
                                rospy.Time.now(),
                                "fly",
                                "base_link")





    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('mechanical_vr_driver_node', anonymous=True)
        rate = rospy.Rate(1000)    # 10 Hz
        node = MechanicalVrDriver()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()
        node.df.closeFile()
    except rospy.ROSInterruptException:
        pass

