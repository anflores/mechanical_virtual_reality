import numpy as np

import astar
from settings import *
from shapely.geometry import Polygon, LineString, Point

import rospkg
rospack = rospkg.RosPack()

def getLineString(wall):
    wall_ = np.zeros([wall.shape[0] + 1, wall.shape[1]])
    wall_[:-1, :] = wall
    wall_[-1, :] = wall[0, :]
    return LineString(wall_)



def min_distance(pt1, pt2, p):
    # return the projection of point p (and the distance) on the closest edge formed by the two points pt1 and pt2

    l = np.sum((pt2 - pt1) ** 2)  ## compute the squared distance between the 2 vertices
    t = np.max([0., np.min([1., np.dot(p - pt1, pt2 - pt1) / l])])  # I let the answer of question 849211 explains this
    proj = pt1 + t * (pt2 - pt1)  ## project the point
    return proj, np.sqrt(np.sum((proj - p) ** 2))  ## return the projection and the point


def getIndexes(pos, x, y):
    dx = float(x[-1] - x[0]) / len(x)
    dy = float(y[-1] - y[0]) / len(y)

    ix = np.where(np.logical_and(pos[0] >= x, pos[0] < x + dx))[0]
    iy = np.where(np.logical_and(pos[1] >= y, pos[1] < y + dy))[0]

    return [ix, iy]


class PathPlanner:
    def __init__(self, collision_file):

        dir = rospack.get_path('mechanical_arena_description') + '/arena_collisions/' + collision_file + '.txt'
        arena_vertices = open(dir, 'r')

        x = []
        y = []

        for line in arena_vertices.readlines():
            x_co, y_co = line.split(' ')
            x.append(float(x_co))
            y.append(float(y_co))

        self.wall = np.stack([x, y]).T

        self.wall = self.dilateWall()


    def dilateWall(self, cap_style=1, join_style=3):
        line = getLineString(self.wall)
        # dilated = line.buffer(dilate_distance, cap_style =cap_style, join_style=join_style)

        dilated = line.parallel_offset(-DISTANCE_COLISION, 'left', join_style=join_style, resolution=1)
        x = dilated.xy[0]
        y = dilated.xy[1]

        wall_dilated = np.array([x, y])

        return wall_dilated.T[:-1]

    def getDistanceFromWall(self, pos):
        convexHull = self.wall
        pos_proj_hulls = []
        dist_hulls = []

        pos_offset = convexHull - pos
        tang_vect = np.zeros(convexHull.shape)
        n = convexHull.shape[0]

        inside = False

        for i in range(n):

            pos1 = convexHull[i % n]
            pos2 = convexHull[(i + 1) % n]

            if pos[1] > min(pos1[1], pos2[1]):
                if pos[1] <= max(pos1[1], pos2[1]):
                    if pos[0] <= max(pos1[0], pos2[0]):
                        if pos1[1] != pos2[1]:
                            xints = (pos[1] - pos1[1]) * (pos2[0] - pos1[0]) / (pos2[1] - pos1[1]) + pos1[0]
                        if pos1[0] == pos2[0] or pos[0] <= xints:
                            inside = not inside

            pos_proj, d = min_distance(pos1, pos2, pos)
            pos_proj_hulls.append(pos_proj)
            dist_hulls.append(d)

            tang_vect[i, :] = pos2 - pos1

        pos_proj_hulls = np.array(pos_proj_hulls)
        ix = np.argmin(dist_hulls)

        res = np.cross(pos_offset, tang_vect)

        # return pos_proj_hulls[ix,:], dist_hulls[ix], inside
        dist = dist_hulls[ix]

        if inside:
            dist = - dist
        # Returns the minimum distnace, the projection to the hull and the tangent vector of the
        return dist, pos_proj_hulls[ix, :], np.true_divide(tang_vect[ix, :], np.linalg.norm(tang_vect[ix, :]))

    def buildWeightMap(self, resolution=30, min_weight=0, max_weight=100000, radius_arena=100, min_dist=-1):
        self.weights = np.zeros([resolution, resolution], dtype=np.float32)
        self.x = np.linspace(-radius_arena, radius_arena, resolution)
        self.y = np.linspace(-radius_arena, radius_arena, resolution)


        for i in range(len(self.x)):
            for j in range(len(self.y)):
                dist, _, _ = self.getDistanceFromWall([self.x[i], self.y[j]])
                if dist > min_dist:
                    dist = max_weight
                self.weights[i, j] = dist

        self.weights = np.true_divide(self.weights - self.weights.min(), self.weights.max() - self.weights.min())

        self.weights = min_weight + (max_weight - min_weight) * self.weights



    def getPath(self, start, goal):
        ix_start = getIndexes(start, self.x, self.y)
        ix_goal  = getIndexes(goal, self.x, self.y)

        ix_start = (ix_start[0][0], ix_start[1][0])
        ix_goal = (ix_goal[0][0], ix_goal[1][0])



        path = astar.search(self.weights, self.weights, ix_start, ix_goal)
        path_coord = np.array ( [ self.x[path[:,0]], self.y[path[:,1]] ] ).T

        return path_coord




if __name__ == '__main__':
    import matplotlib.pyplot as plt

    pathPlanner = PathPlanner('arena_maze')
    pathPlanner.buildWeightMap(radius_arena = DIAMETER_ARENA/2, min_dist = -2)


    path = pathPlanner.getPath([-30,-20], [-30, 20])
    fig, axis = plt.subplots(figsize=(10, 10))

    axis.imshow(pathPlanner.weights.T, aspect='auto', origin='lower', extent=[pathPlanner.x[0], pathPlanner.x[-1], pathPlanner.y[0], pathPlanner.y[-1]], zorder=-1)
    plt.plot(path[:, 0], path[:, 1], linewidth=5, color='red')
    plt.show()