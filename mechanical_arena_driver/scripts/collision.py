import numpy as np
from settings import *
from shapely.geometry import Polygon, LineString, Point


import rospkg
rospack = rospkg.RosPack()


def getAngleBetweenVect(vector_1, vector_2):
    unit_vector_1 = np.true_divide(vector_1, np.linalg.norm(vector_1))
    unit_vector_2 = np.true_divide(vector_2, np.linalg.norm(vector_2))
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle = np.arccos(dot_product)
    return angle


def increaseResolution(wall, iterations=1, min_angle=60):
    for i in range(iterations):
        wall = increaseResolution1iter(wall, min_angle)

    return wall


def increaseResolution1iter(wall, min_angle=60):
    wall_highres = []
    n = wall.shape[0]
    for i in range(n):
        j = (i + 1) % n
        k = (i - 1) % n

        edge1 = wall[j] - wall[i]
        edge2 = wall[k] - wall[i]

        angle = getAngleBetweenVect(edge1, edge2)
        angle = np.degrees(angle) % 360
        if np.isnan(angle):
            continue

        mid_point = np.true_divide(wall[i] + wall[j], 2)
        if angle > min_angle:
            wall_highres.append(wall[i])
        wall_highres.append(mid_point)

    return np.array(wall_highres)

def min_distance(pt1, pt2, p):
    # return the projection of point p (and the distance) on the closest edge formed by the two points pt1 and pt2

    l = np.sum((pt2 - pt1) ** 2)  ## compute the squared distance between the 2 vertices
    t = np.max([0., np.min([1., np.dot(p - pt1, pt2 - pt1) / l])])  # I let the answer of question 849211 explains this
    proj = pt1 + t * (pt2 - pt1)  ## project the point
    return proj, np.sqrt(np.sum((proj - p) ** 2))  ## return the projection and the point


def getLineString(wall):
    wall_ = np.zeros([wall.shape[0] + 1, wall.shape[1]])
    wall_[:-1, :] = wall
    wall_[-1, :] = wall[0, :]
    return LineString(wall_)



def normalizeVector(vect):
    return np.true_divide(vect, np.linalg.norm(vect))


class Collision:
    def __init__(self, collision_file = None):
        self.wall = None

        if collision_file is not None:
            dir = rospack.get_path('mechanical_arena_description') + '/arena_collisions/' + collision_file + '.txt'
            arena_vertices = open(dir, 'r')

            x = []
            y = []

            for line in arena_vertices.readlines():
                x_co, y_co = line.split(' ')
                x.append(float(x_co))
                y.append(float(y_co))

            self.wall = np.stack([x, y]).T

            self.wall = self.dilateWall(-DISTANCE_COLISION)
            self.wall = increaseResolution(self.wall, iterations= 2, min_angle = 0)
            self.wall = increaseResolution(self.wall, iterations= 20, min_angle = 350)
            self.wall = increaseResolution(self.wall, iterations=2, min_angle=0)

            self.wall_mm = np.true_divide(self.wall, 1000)  # To mm

            self.getNormals()

    def getNormals(self):
        #### Compute normals to edges ######
        self.normals = np.zeros(self.wall.shape)
        self.normals_center = np.zeros(self.wall.shape)

        n = self.wall.shape[0]
        for i in range(n):
            j = (i + 1) % n

            edge_vec = self.wall[j] - self.wall[i]
            if np.any(edge_vec):
                self.normals[i, :] = normalizeVector([edge_vec[1], -edge_vec[0]])
            else:
                self.normals[i, :] = self.normals[(i - 1) % n, :]

            self.normals_center[i, :] = np.true_divide(self.wall[j] + self.wall[i], 2)



    def getInterpNormal(self, pos):
        n = self.wall.shape[0]


        pos_offset = self.normals_center - pos
        dist_to_center_edge = np.sqrt(np.power(pos_offset, 2).sum(axis=1))
        ix_min = np.argmin(dist_to_center_edge)
        closest_center_edge = self.normals_center[ix_min]

        second_closest = self.normals_center[[(ix_min - 1) % n, (ix_min + 1) % n], :]
        dist_2_second_closest = np.sqrt(np.power(second_closest - pos, 2).sum(axis=1))
        ix_second_closest = np.argmin(dist_2_second_closest)

        if ix_second_closest == 0:
            ix_second_closest = (ix_min - 1) % n
        else:
            ix_second_closest = (ix_min + 1) % n

        second_closest_center_edge = self.normals_center[ix_second_closest]

        weights = dist_to_center_edge[[ix_min, ix_second_closest]]
        weights = np.true_divide(weights, weights.sum())

        normal_interp = self.normals[ix_min] * (1 - weights[0]) + self.normals[ix_second_closest] * (1 - weights[1])
        return normalizeVector( normal_interp )





    def isInside(self, pos):
        convexHull = self.wall
        n = convexHull.shape[0]

        inside = False

        for i in range(n):
            pos1 = convexHull[i % n]
            pos2 = convexHull[(i + 1) % n]

            if pos[1] > min(pos1[1], pos2[1]):
                if pos[1] <= max(pos1[1], pos2[1]):
                    if pos[0] <= max(pos1[0], pos2[0]):
                        if pos1[1] != pos2[1]:
                            xints = (pos[1] - pos1[1]) * (pos2[0] - pos1[0]) / (pos2[1] - pos1[1]) + pos1[0]
                        if pos1[0] == pos2[0] or pos[0] <= xints:
                            inside = not inside
        return inside




    def getDistanceFromWall(self, pos, min_dist_from_vertex = 10):
        convexHull = self.wall

        pos_proj_hulls = []
        dist_hulls = []

        pos_offset = convexHull - pos
        tang_vect = np.zeros(convexHull.shape)
        n = convexHull.shape[0]

        inside = False

        for i in range(n):
            pos1 = convexHull[i % n]
            pos2 = convexHull[(i + 1) % n]

            if pos[1] > min(pos1[1], pos2[1]):
                if pos[1] <= max(pos1[1], pos2[1]):
                    if pos[0] <= max(pos1[0], pos2[0]):
                        if pos1[1] != pos2[1]:
                            xints = (pos[1] - pos1[1]) * (pos2[0] - pos1[0]) / (pos2[1] - pos1[1]) + pos1[0]
                        if pos1[0] == pos2[0] or pos[0] <= xints:
                            inside = not inside

            pos_proj, d = min_distance(pos1, pos2, pos)
            pos_proj_hulls.append(pos_proj)
            dist_hulls.append(d)

            tang_vect[i, :] = pos2 - pos1

        pos_proj_hulls = np.array(pos_proj_hulls)
        ix = np.argmin(dist_hulls)

        res = np.cross(pos_offset, tang_vect)

        # return pos_proj_hulls[ix,:], dist_hulls[ix], inside
        dist = dist_hulls[ix]

        if inside:
            dist = - dist
            # Returns the minimum distnace, the projection to the hull and the tangent vector of the
        return dist, pos_proj_hulls[ix, :], np.true_divide(tang_vect[ix, :], np.linalg.norm(tang_vect[ix, :]))





    def clipVelocity(self, posMotors, velMotors, min_dist_from_vertex = 5):
        posMotors = [-posMotors[0], -posMotors[1]]
        velMotors = [-velMotors[0], -velMotors[1]]


        if self.wall is not None:        # If wall is defined
            if not self.isInside(posMotors):

                norm_vector = self.getInterpNormal(posMotors)

                if np.dot(norm_vector, velMotors) > 0:
                    tang_vector = np.array([-norm_vector[1], norm_vector[0]])
                    vel_tang_arena = np.dot(velMotors, tang_vector) * tang_vector  # Project to tangent vector
                    #print("tangent vector", tang_vector)
                    #print("motorVel", velMotors)
                    #print("clip velocity", vel_tang_arena)
                    #print("   ")
                    return np.array( [-vel_tang_arena[0], -vel_tang_arena[1]] )


            '''
            dist, proj_pos, tang_vect = self.getDistanceFromWall(posMotors)

            if dist > 0:  # outside
                norm_vect = np.array([-tang_vect[1], tang_vect[0]])
                if np.dot(norm_vect, velMotors) > 0:
                    vel_tang_arena = np.dot(velMotors, tang_vect) * tang_vect  # Project to tangent vector
                    return vel_tang_arena
            '''
            return np.array([-velMotors[0], -velMotors[1]])

        else:                            # If not, just assume a circular arena
            dist_from_origin = np.linalg.norm(posMotors)
            max_dist_from_origin = float(DIAMETER_ARENA) / 2 - WIDTH_ARENA - DISTANCE_COLISION
            if dist_from_origin > max_dist_from_origin:
                ang_pos_arena = np.arctan2(posMotors[1], posMotors[0])
                norm_vector_arena = np.array([np.cos(ang_pos_arena), np.sin(ang_pos_arena)])
                if np.dot(norm_vector_arena, np.array(velMotors)) > 0:
                    tang_vector_arena = np.array([np.sin(ang_pos_arena), - np.cos(ang_pos_arena)])
                    vel_tang_arena = np.dot(np.array(velMotors), tang_vector_arena) * tang_vector_arena
                    return np.array( [-vel_tang_arena[0], -vel_tang_arena[1]] )

            return np.array([-velMotors[0], -velMotors[1]])

    def dilateWall(self, dilate_distance, cap_style=1, join_style=3):
        line = getLineString(self.wall)
        # dilated = line.buffer(dilate_distance, cap_style =cap_style, join_style=join_style)

        dilated = line.parallel_offset(-DISTANCE_COLISION, 'left', join_style=join_style, resolution=1)
        x = dilated.xy[0]
        y = dilated.xy[1]

        wall_dilated = np.array([x, y])

        return wall_dilated.T


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    col = Collision('arena_maze')

    plt.figure()
    plt.scatter(col.wall[:,0], col.wall[:,1])
    plt.show()