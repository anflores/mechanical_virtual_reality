#ifndef STEPPER_MOTOR_H
#define STEPPER_MOTOR_H



#include "Arduino.h"



enum motionMode {
 MOVE_SPEED,
 MOVE_STEPS,
 STOPPED
};


class StepperMotor{
  public:
	StepperMotor(short id, short stepPin, short dirPin);
	
	void moveSpeed(int speed);
	void move2Position(long reqStep, int speed);
	void command(long reqStep, int speed);
	void run();
	void stopMotor();
	short getId(){return this->id;};
	long getCurrentStepPosition(){return this->currentStep;};
	long getSpeed(){return this->currentSpeed;};
	motionMode getState(){return this->STATE;};


	
	

  private:
	void runSingleStep();
	void setDir(bool dir);

	
	short id;
	short stepPin;
	short dirPin;


	bool stepState;

	long currentStep;
	long requestedStep;
	bool currentDir;
	int timeSteps;
	long currentSpeed;

	motionMode STATE;
	long timeStar;
	

};


#endif // STEPPER_MOTOR_H
