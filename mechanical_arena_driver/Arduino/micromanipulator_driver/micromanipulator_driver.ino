#include "SerialTransfer.h"
#include <stepper_motor.h>
#include <ODriveArduino.h>
// Printing with stream operator
template<class T> inline Print& operator <<(Print &obj,     T arg) { obj.print(arg);    return obj; }
template<>        inline Print& operator <<(Print &obj, float arg) { obj.print(arg, 4); return obj; }






#define NUM_MOTORS 2
String DRIVER_NAME = "mechanical_arena_xy";
char *motorNames[] = {"X", "Y"};






/************** DEFINE MOTOR SPEED **************/
#define MIN_SPEED 50  // steps/sec
#define MAX_SPEED 500 // steps/sec






/************** DEFINE PINS **************/
#define ENABLE_PIN    8

short PIN_STEP[NUM_MOTORS] = {7,6};
short PIN_DIR[NUM_MOTORS]  = {4,3};




/***** REMEMBER THAT DEPENDING ON THE ARDUINO BOARD, THE SIZE OF int OR long CHANGE! ****/

struct STATEMSG {
  int position[NUM_MOTORS] = {0};
  int velocity[NUM_MOTORS] = {0};
};





enum DRIVERSTATE {stopped, working};
DRIVERSTATE driverState;





STATEMSG cmdMsg;
STATEMSG stateMsg;

StepperMotor* motors[NUM_MOTORS];

long timerPublisher;







/***** SERIAL COMMUNCATION *******/
String inputString = "";         // string to hold incoming data
bool stringComplete = false;     // whether the string is complete
SerialTransfer serialCom;
float PUBLISH_DATA_FREQ;











void sendMotorStates(){
  long delayTime = millis() - timerPublisher ;

  if (delayTime > 1000/PUBLISH_DATA_FREQ){
    
        for(short i=0; i < NUM_MOTORS; i++){
            stateMsg.position[i] = motors[i]->getCurrentStepPosition();
            stateMsg.velocity[i] = motors[i]->getSpeed();
        }
        
        uint16_t sendSize = 0;
        serialCom.txObj(stateMsg, sizeof(stateMsg), sendSize);
        sendSize += sizeof(stateMsg);
      
        // Send buffer
        serialCom.sendData(sendSize);
        timerPublisher = millis();
    }
}


void setup()
{
  // ODrive uses 115200 baud
  Serial1.begin(115200);
  
  SerialUSB.begin(115200);

  
  pinMode(ENABLE_PIN, OUTPUT);
  digitalWrite(ENABLE_PIN, LOW);

  for(short i=0; i < NUM_MOTORS; i++){
      motors[i] = new StepperMotor(i, PIN_STEP[i], PIN_DIR[i]);
  }

  timerPublisher = millis();

  driverState = stopped;
  
  //motors[0]->command(10000, 100);
}


void loop(){
  if (driverState == working){
    getMotorCmds();
    for(short i=0; i < NUM_MOTORS; i++){
        motors[i]->run();
    }
    sendMotorStates(); 
  }
  
  else{
    serialInput();
    serialCB();
  }
}



void getMotorCmds(){
  if(serialCom.available()){
        uint16_t recSize = 0;
        serialCom.rxObj(cmdMsg, sizeof(cmdMsg), recSize);
        for(short i=0; i < NUM_MOTORS; i++){
            motors[i]->command(cmdMsg.position[i], cmdMsg.velocity[i]);
        }
    }
  else{
    //Serial.print("ERROR: ");
    //if(serialCom.status == -1)
    //  Serial.println(F("CRC_ERROR"));
    //else if(serialCom.status == -2)
    //  Serial.println(F("PAYLOAD_ERROR"));
    //else if(serialCom.status == -3)
    //  Serial.println(F("STOP_BYTE_ERROR"));
  }

  //delayMicroseconds(1);
}




void serialCB(){
  if (stringComplete){

    if (inputString.substring(0,4) == String("info")){
      String infoMsg = DRIVER_NAME + String(",") + String(NUM_MOTORS);
      for(short i=0; i < NUM_MOTORS; i++){
        infoMsg +=  String(",") + String(motorNames[i]);
      }
      SerialUSB.println(infoMsg);
      delay(5);
      
    }
    else if (inputString.substring(0,5) == String("start")){
      PUBLISH_DATA_FREQ = inputString.substring(5).toFloat();
      driverState = working;
      serialCom.begin(SerialUSB);
    }



    inputString = "";
    stringComplete = false;
  }
    
}



void serialInput() {
  while (SerialUSB.available()) {
    // get the new byte:
    char inChar = (char)SerialUSB.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
